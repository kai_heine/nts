﻿/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#include <iostream>
#include <thread>
#include <csignal>
#include <openssl/crypto.h>
#include "../../nts/core/src/Logger/Logger.h"
#include <numeric> //iota
#include "NtpPacketInfo.h"
#include "NtsUnicastService.h"
#include <cstddef>
#include <vector>


#ifdef _MSC_VER
//enable debug
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG // TODO: use NDEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#endif


void my_handler(int s)
{
	std::cout << "Caught signal: " << s << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	exit(1);
}

int main()
{
#ifdef _MSC_VER
	// find memory leaks
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//_CrtSetBreakAlloc(23609);
#endif

	static_assert(sizeof(unsigned short) == 2, "unsigned short != 2");
	static_assert(CHAR_BIT == 8, "CHAR_BIT != 8");

	std::signal(SIGINT, my_handler);


	std::vector<uint8_t> ntp(48);
	std::iota(ntp.begin(), ntp.end(), std::uint8_t{ 0 }); // befüllen von ntp 0, 1, 2, ...
	int ntpPacketLen = static_cast<int>(ntp.size());

	try
	{

		NtsUnicastService ntsClient;
		ntsClient.initialize("configClient.ini");
		ntsClient.showInfos();

		NtsUnicastService ntsServer;
		ntsServer.initialize("configServer.ini");


		// ========================================================================================
		// CLIENT
		// ========================================================================================
		printf("\n\nCLIENT:\n-----------------\n");

		NtpPacketInfo pkifo1;
		pkifo1.setServer("127.0.0.1", "127.0.0.2", 123);


		// NTS-Nachricht erstellen (request): INIT
		int result = ntsClient.createMessage_init(pkifo1);
		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_init()': required space for NTS content: %d bytes\n", result);
			// extend ntp packet buffer
			if (result > 0)
			{
				size_t newSize = ntp.size() + result;
				ntp.resize(newSize);
				printf("NTP packet resized! New buffer length: %lu bytes\n", ntp.size());
			}
		}



		// NTS-Nachricht erstellen (request): UPDATE
		result = ntsClient.createMessage_update(pkifo1, ntp.data(), ntp.size(), ntpPacketLen);
		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_update()': current NTP packet size: %d bytes\n", result);
			ntpPacketLen = result;
		}
		


		// ==================================== TIME CRIT ====================================
		auto start = std::chrono::steady_clock::now();
		// -----------------------------------------------------------------------------------

		// NTS-Nachricht erstellen (request): FINAL
		result = ntsClient.createMessage_final(pkifo1, ntp.data(), ntp.size(), ntpPacketLen, false);

		// -----------------------------------------------------------------------------------
		auto end = std::chrono::steady_clock::now();
		auto time = end - start;
		std::chrono::duration<double, std::micro> proccess_time_us = time;
		LOG_FATAL("crit time: {} us", proccess_time_us.count());
		// ==================================== TIME CRIT ====================================

		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_final()': current NTP packet size: %d bytes\n", result);
			ntpPacketLen = result;
		}









		// auslesen, an welchen NTP server das NTP gesendet werden soll
		//pkifo.getTimeServerIp();
		//pkifo.getTimeServerPort();

		// ========================================================================================
		// SERVER
		// ========================================================================================
		printf("\n\nSERVER:\n-----------------\n");

		int ntsSpaceAvailable = 0;
		NtpPacketInfo pkifo2;

		// NTS-Nachricht überprüfen und parsen
		result = ntsServer.processAndVerify(pkifo2, ntp.data(), ntp.size(), ntpPacketLen);

		if (result < 0)
		{
			printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
			ntsSpaceAvailable = result;
		}



		// NTS-Nachricht erstellen (response): INIT
		result = ntsServer.createMessage_init(pkifo2);
		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_init()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_init()': required space for NTS content: %d bytes\n", result);

			// extend ntp packet buffer (if necessary) 
			if ((result - ntsSpaceAvailable) > 0)
			{
				size_t newSize = ntp.size() + result;
				ntp.resize(newSize);
				printf("NTP packet resized! New buffer length: %lu bytes\n", ntp.size());
			}
		}



		// NTS-Nachricht erstellen (request): UPDATE
		result = ntsServer.createMessage_update(pkifo2, ntp.data(), ntp.size(), ntpPacketLen);
		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_update()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_update()': current NTP packet size: %d bytes\n", result);
			ntpPacketLen = result;
		}



		// NTS-Nachricht erstellen (request): FINAL
		result = ntsServer.createMessage_final(pkifo2, ntp.data(), ntp.size(), ntpPacketLen, false);
		if (result < 0)
		{
			printf("[FAIL] 'NTS_create_message_final()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_create_message_final()': current NTP packet size: %d bytes\n", result);
			ntpPacketLen = result;
		}



		// ========================================================================================
		// CLIENT
		// ========================================================================================

		printf("\n\nCLIENT:\n-----------------\n");
		NtpPacketInfo pkifo3;
		pkifo3.setServer("", "127.0.0.2", 0); // IP-Adresse des NTP Servers (um die NTS-Nachricht zuzuordnen)


		// NTS-Nachricht überprüfen und parsen
		result = ntsClient.processAndVerify(pkifo3, ntp.data(), ntp.size(), ntpPacketLen);

		if (result < 0)
		{
			printf("[FAIL] 'NTS_process_and_verify()':  error code: %d\n", result);
		}
		else
		{
			printf("[SUCCESS] 'NTS_process_and_verify()': current NTS content size: %d bytes\n", result);
			ntsSpaceAvailable = result;
		}

	}
	catch (std::exception& e)
	{
		std::cout << "EXCEPTION: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "unknown EXCEPTION" << std::endl;
	}
	

	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	std::cout << "Programmende erreicht\n\n----------------------------------\nProgramm beendet" << std::endl;
	std::cin.get();
	return 0;
}
