#include "NtsClient.h"
#include "../NtpPacketInfoImpl.h"
#include "../Logger/Logger.h"

NtsClient::NtsClient(NtsSettings const& settings)
	:
	m_settings(settings)
{
}



NtsClientSession & NtsClient::getSession(std::string const & keServerIp)
{
	// search for existing sessions
	for (auto& it : m_clientSessionList)
	{
		if (it.getKeServerIp() == keServerIp)
		{
			LOG_TRACE("client session found");
			return it;
		}
	}
	
	// session not found; create a new one
	LOG_TRACE("client session not found; create a new one");
	m_clientSessionList.emplace_back(m_settings, keServerIp);
	if (m_clientSessionList.empty() == false)
	{
		return m_clientSessionList.back();
	}
	else
	{
		LOG_FATAL("can not create a new session");
		throw std::runtime_error("can not create a new session");
	}
}



void NtsClient::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{
	std::string const & ntpServerIP = ntpPacketInfo.getNtpTimeServerIp();
	if (ntpServerIP == "")
	{
		LOG_ERROR("NtsClient::receivedNtpResponse(): ntpServerIP is empty");
		return;
	}
	LOG_TRACE("NtsClient: IP address (ntp time server): {}", ntpServerIP);
	NtsClientSession & clientSession = getSession(ntpServerIP);
	clientSession.processAndVerify(ntpPacketInfo);
}


void NtsClient::createRequest(NtpPacketInfoImpl & ntpPacketInfo, int stage)
{
	std::string const & ntpTimeServerIp = ntpPacketInfo.getNtpTimeServerIp();
	if (ntpTimeServerIp == "")
	{
		LOG_ERROR("ntpTimeServerIp is empty");
		// todo set error
		return;
	}
	LOG_TRACE("IP address (ntp time server): {}", ntpTimeServerIp);
	NtsClientSession & clientSession = getSession(ntpTimeServerIp);
	clientSession.createRequest(ntpPacketInfo, stage);
}
