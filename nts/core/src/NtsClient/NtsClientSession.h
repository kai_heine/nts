/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once


#include <string>
#include "./NtsClientKe.h"
#include "ClientState.h"
#include "NtsClientNtp.h"


struct NtpPacketInfoImpl;
struct NtsSettings;


class NtsClientSession
{
public:
	NtsClientSession() = delete;
	NtsClientSession(NtsSettings const& settings, std::string const& keServerIp);
	~NtsClientSession() = default;

	std::string const & getKeServerIp() const;
	void processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createRequest(NtpPacketInfoImpl& ntpPacketInfo, int stage);


private:


	// TLS connection
	void startTlsKe(std::string const& keServerIp, int port);
	void processTlsResponsePostProcess();

	std::string m_keServerIp;
	NtsSettings const & m_settings;

	
	NtsClientKe m_ntsTlsKe;
	NtsClientNtp m_ntpProtocol;
	ClientState m_clientState;
	
	// int lastActivity -->  (lifetime)


};

