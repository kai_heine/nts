/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
#include "../BaseConfig.h"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <cstddef>

class NtsClientKe;
class TlsClient
{
  public:
	TlsClient(boost::asio::io_service &ioService,
	          boost::asio::ssl::context &context,
	          boost::asio::ip::tcp::resolver::iterator endpointIterator,
	          NtsClientKe &ntsClientKe);

	~TlsClient();

	bool verifyCertificate(bool preverified, boost::asio::ssl::verify_context &ctx);
	void connectHandler(const boost::system::error_code &error);
	void handshakeHandler(const boost::system::error_code &error);
	void writeHandler(const boost::system::error_code &error, std::size_t bytesTransferred);
	void readHandler(const boost::system::error_code &error, std::size_t bytesTransferred);
	void shutdownHandler(const boost::system::error_code &error);

	static void cb_ssl_msg(int write_p, int version, int content_type, const void *buf, std::size_t len, SSL *ssl, void *arg);

  private:
	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> m_socket;
	std::vector<unsigned char> m_responseBuffer;
	std::vector<unsigned char> m_alpnList;

	NtsClientKe &m_ntsClientKe;
};
