#include "NtsClientNtp.h"
#include "../AEAD.h"
#include ".././NtpPacket.h"
#include "../NtpPacketInfoImpl.h"
#include <sstream>
#include <cstddef>
//#include <assert.h>
//#include <iostream>
//
#include "../Logger/Logger.h"
//#include "../generalFunctions.h"
//#include "../cryptoFunctions.h"
//#include "../NtsConfig.h"
#include "../NtpExtensionFields/NtpExtensionField.h"
#include "../NtpExtensionFields/ExtF_UniqueIdentifier.h"
#include "../NtpExtensionFields/ExtF_NtsAuthAndEncEF.h"
#include "../NtpExtensionFields/ExtF_NtsCookie.h"
#include "../NtpExtensionFields/ExtF_NtsCookiePlaceholder.h"
#include <chrono>
//#include "../NtpExtensionFields/NtpExtFieldStack.h"
#include "../NtpExtensionFields/NtpExtFieldStackVerification.h"
#include "../NtsSettings.h"
#include "ClientState.h"

NtsClientNtp::NtsClientNtp(NtsSettings const& settings, ClientState& clientState)
    : 
	m_settings(settings),
	m_clientState(clientState)
{
}


bool NtsClientNtp::verifyNtpMessage(NtpPacket &ntpPacket, NtpExtensionField const &aeadExtField)
{
	bool status = true;

	if (ExtField::NtsAuthAndEncEF::isValid(aeadExtField) == false)
	{
		LOG_ERROR("NtsClientNtp::verifyNtpMessage(): aeadExtField is invalid");
		status = false;
	}

	std::vector<unsigned char> nonce = ExtField::NtsAuthAndEncEF::getNonce(aeadExtField);
	std::vector<unsigned char> encryptedContent = ExtField::NtsAuthAndEncEF::getCiphertext(aeadExtField);

	//serialisieren bis zum AEAD-Offset (nichts l�schen)


	// TODO das sollte ge�ndert werden
	int extFieldCount = ntpPacket.getExtFieldStack().getStackSize();
	for (int index = 1; index <= extFieldCount; index++)
	{
		if (ntpPacket.getExtFieldStack().getExtField(index).getFieldType() == NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields)
		{
			ntpPacket.getExtFieldStack().delExtField(index); // TODO: �ndern
			break;
		}
	}

	// check integrity and encrypt
	AEAD aead;
	aead.setAeadAlgorithm(m_clientState.negAeadAlgo);
	aead.setKey(m_clientState.S2CKey);
	aead.setCiphertext(encryptedContent);
	aead.setNonce(nonce);
	aead.setAssociatedData(ntpPacket.getSerializedPackage());

	if (aead.decrypt() == false)
	{
		LOG_ERROR("NtsClientNtp::verifyNtpMessage: decryption failed");
		status = false;
	}

	// save decrypted ext fields (if available)
	
	m_clientState.decryptedExtFields = aead.getPlaintext();

	return status;
}


bool NtsClientNtp::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{

	LOG_TRACE("");
	LOG_TRACE("Client: procNtpRes: STARTED");


	// set error codes
	ntpPacketInfo.setErrorCode(-1);

	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();
	LOG_TRACE("Client: procNtpRes: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Client: procNtpRes: number of NTP EFs: {}", extFieldStack.getStackSize());
	std::size_t existingContentSize = 0;


	// ADVANCED TRACE OUT
	int numEFs = extFieldStack.getStackSize();
	std::vector<unsigned char> rawNtpPacket = ntpPacket.getSerializedPackage();
	LOG_TRACE("Client: procNtpRes: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		numEFs);

	for (int i = 1; i <= numEFs; i++)
	{
		LOG_TRACE("Client: procNtpRes: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			extFieldStack.getExtField(i).getFieldTypeVal(),
			extFieldStack.getExtField(i).getValueLength(),
			getItemID(extFieldStack.getExtField(i).getValue())
		);
	}
	// ADVANCED TRACE OUT

	// Erweiterungsfelder extrahieren
	int numReceivedExtFields = extFieldStack.getStackSize();
	NtpExtFieldStack receivedExtFieldStack;
	if (numReceivedExtFields == 0)
	{
		LOG_ERROR("Client: procNtpRes: received NTP packet does not include any EFs");
		return false;
	}
	for (int index = 1; index <= numReceivedExtFields; index++)
	{
		NtpExtensionField ntpExtF;
		ntpExtF.setFieldType(extFieldStack.getExtField(index).getFieldTypeVal());
		ntpExtF.setValue(extFieldStack.getExtField(index).getValue());
		receivedExtFieldStack.addExtField(ntpExtF);
	}

	// ToDo: NTS cookies are WITHIN the AuthAndEncExt!
	// ToDo: was tun wenn cookies nicht in aead ext sind?
	// check record stack (is it NTS conform?)
	bool receivedNAK = false; // todo: anpassen
	if (NtpExtFieldStackVerification().isStackValid(receivedExtFieldStack, NtpMode::Server_mode, true, receivedNAK) == false)
	{
		LOG_ERROR("Client: procNtpRes: wrong NTP EF constellation");
		return false;
	}

	// check unique ID (must match)
	unsigned int uniqIdIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	const NtpExtensionField& uniqIdExtField = receivedExtFieldStack.getExtField(uniqIdIndex);
	existingContentSize += uniqIdExtField.getExtensionFieldLength();
	const std::vector<unsigned char>& receivedUID = ExtField::UniqueIdentifier::getUniqueId(uniqIdExtField);
	if (receivedUID != m_clientState.uniqueIdentifier)
	{
		LOG_ERROR("Client: procNtpRes: unique ID mismatch");
		return false;
	}
	m_clientState.uniqueIdentifier.clear();
	LOG_TRACE("Client: procNtpRes: extracted uniqueID (ID: {}, size: {})",
		getItemID(receivedUID),
		receivedUID.size());

	// Integrit�tspr�fung der empfangenen Nachricht
	unsigned int aeadExtFieldIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
	const NtpExtensionField& extractedAeadEF = receivedExtFieldStack.getExtField(aeadExtFieldIndex);
	existingContentSize += extractedAeadEF.getExtensionFieldLength();
	if (verifyNtpMessage(ntpPacket, extractedAeadEF) == false)
	{
		LOG_ERROR("Client: procNtpRes: Integrity check failed");
		return false;
	}

	// FOR TRACE
	rawNtpPacket = ntpPacket.getSerializedPackage();
	LOG_TRACE("Client: procNtpRes: NTP Packet (after AEAD --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		receivedExtFieldStack.getStackSize());

	// ggf. verschl�sselte EF verarbeiten
	if (m_clientState.decryptedExtFields.size() != 0)
	{
		LOG_TRACE("Client: procNtpRes: start parsing decrypted extension fields");

		NtpExtFieldStack decryptedExtFieldStack(m_clientState.decryptedExtFields);
		LOG_TRACE("Client: procNtpRes: decrypted EF (amount): {}", decryptedExtFieldStack.getStackSize());

		// Cookie(s) extrahieren und speichern
		int countCookies = 0;
		for (unsigned int index = 1; index <= decryptedExtFieldStack.getStackSize(); index++)
		{
			const NtpExtensionField& extField = decryptedExtFieldStack.getExtField(index);
			if (extField.getFieldType() == NtpExtFieldType::NTS_Cookie)
			{
				if (ExtField::NtsCookie::isValid(extField) == false)
				{
					LOG_ERROR("Client: procNtpRes: extracted raw Cookie is invalid");
					return false;
				}
				else
				{
					m_clientState.cookieStack.emplace_back(ExtField::NtsCookie::getCookie(extField));
					countCookies++;
					LOG_TRACE("Client: procNtpRes: pushed received cookie into stack: ID: {}", getItemID(ExtField::NtsCookie::getCookie(extField)));
				}
			}
		}
		if (countCookies == 0)
		{
			LOG_ERROR("Client: procNtpRes: no cookies received");
			return false;
		}

		// FOR TRACE: list cookies
		std::deque<std::vector<unsigned char>> tempCookieStack(m_clientState.cookieStack);
		LOG_TRACE("Client: procNtpRes: amount of cookies (after extract): {}", tempCookieStack.size());
		std::stringstream CookieStackStr;
		while (tempCookieStack.empty() == false)
		{
			CookieStackStr << getItemID(tempCookieStack.front()) << "  ";
			tempCookieStack.pop_front();
		}
		LOG_TRACE("Client: procNtpRes: cookie stack: {}", CookieStackStr.str());
	}	
	LOG_TRACE("Client: procNtpRes: FINISHED");
	LOG_TRACE("");

	// set time :)
	// success
	ntpPacketInfo.setErrorCode(0);
	ntpPacketInfo.setCurrentNtsContentLength(existingContentSize);
	return true;


}







void NtsClientNtp::createRequest(NtpPacketInfoImpl & ntpPacketInfo, int stage)
{
	if (stage == 1)
	{
		LOG_TRACE("");
		LOG_TRACE("Client: genNtpReq: STARTED");
		LOG_TRACE("Client: genNtpReq: STAGE 1");
		m_clientState.clearMessageState();
		createRequest_init(ntpPacketInfo);
	}
	else if (stage == 2)
	{
		LOG_TRACE("Client: genNtpReq: STAGE 2");
		if (m_clientState.messageState == MessageState::uninitialized)
		{
			LOG_WARN("Client: genNtpReq: invalid message state");
			ntpPacketInfo.setErrorCode(-1);
		}
		else
		{
			createRequest_update(ntpPacketInfo);
		}
		
	}
	else if (stage == 3)
	{
		LOG_TRACE("Client: genNtpReq: STAGE 3");
		if ((m_clientState.messageState == MessageState::uninitialized) || (m_clientState.messageState == MessageState::init_complete))
		{
			LOG_WARN("Client: genNtpReq: invalid message state");
			ntpPacketInfo.setErrorCode(-1);
		}
		else
		{
			createRequest_final(ntpPacketInfo);
		}

	}
	else
	{
		LOG_WARN("Client: genNtpReq: invalid message state");
	}
}





bool NtsClientNtp::createRequest_init(NtpPacketInfoImpl & ntpPacketInfo)
{
	// set error codes
	ntpPacketInfo.setErrorCode(-1);
	m_clientState.messageState = MessageState::uninitialized;


	// create EF: Unique Identifier
	NtpExtensionField &uniqIdentEF = m_clientState.uidExtField;
	ExtField::UniqueIdentifier::format(uniqIdentEF);
	m_clientState.uniqueIdentifier = getRandomNumber(UNIQUE_IDENTIFIER_SIZE);
	ExtField::UniqueIdentifier::setUniqueId(uniqIdentEF, m_clientState.uniqueIdentifier);
	LOG_TRACE("Client: genNtpReq: uniqueID extension created (ID: {}, size: {})",
		getItemID(ExtField::UniqueIdentifier::getUniqueId(uniqIdentEF)),
		ExtField::UniqueIdentifier::getUniqueIdSize(uniqIdentEF));


	// FOR TRACE: list cookies
	CONDITIONAL_TRACE
	(
		std::deque<std::vector<unsigned char>> tempCookieStack(m_clientState.cookieStack);
		LOG_TRACE("Client: genNtpReq: amount of cookies: {}", tempCookieStack.size());
		std::stringstream CookieStackStr;
		while (tempCookieStack.empty() == false)
		{
			CookieStackStr << getItemID(tempCookieStack.front()) << "  ";
			tempCookieStack.pop_front();
		}
		LOG_TRACE("Client: genNtpReq: cookie stack: {}", CookieStackStr.str());
	);


	// create EF: NTS Cookie
	NtpExtensionField &ntsCookie = m_clientState.cookieExtField;
	ExtField::NtsCookie::format(ntsCookie);
	if (m_clientState.cookieStack.size() == 0)
	{
		LOG_ERROR("NtsClientNtp::generateNtpRequest(): out of cookies");
		m_clientState.clearNegotiationState();
		return false;
	}
	ExtField::NtsCookie::setCookie(ntsCookie, m_clientState.cookieStack.front());
	m_clientState.cookieStack.pop_front();
	LOG_TRACE("Client: genNtpReq: ntsCookie extension created (cookie ID: {}, size: {})",
		getItemID(ExtField::NtsCookie::getCookie(ntsCookie)),
		ExtField::NtsCookie::getCookieSize(ntsCookie));


	// create EF: NTS Cookieplaceholder (on demand)
	if (m_clientState.cookieStack.size() >= m_settings.client.maxAmountOfCookies)
	{
		LOG_WARN("Client: genNtpReq: cookies available: {}, max. cookies allowed: {}", m_clientState.cookieStack.size() + 1, m_settings.client.maxAmountOfCookies);
		m_clientState.numPlaceholder = 0;
	}
	else
	{
		m_clientState.numPlaceholder = m_settings.client.maxAmountOfCookies - m_clientState.cookieStack.size() - 1;
		LOG_TRACE("Client: genNtpReq: cookiePlaceholder needed: {}", m_clientState.numPlaceholder);

		if (m_clientState.numPlaceholder > 0)
		{
			unsigned short placeholderSize = m_clientState.placeholderSize;
			if (placeholderSize == 0)
			{
				LOG_ERROR("Client: genNtpReq: size of Cookie Placeholder is zero");
				m_clientState.numPlaceholder = 0;
			}
			else
			{
				NtpExtensionField &placeholder = m_clientState.placeholderExtField;
				ExtField::NtsCookiePlaceholder::format(placeholder);
				ExtField::NtsCookiePlaceholder::setPlaceholderSize(placeholder, placeholderSize);
				LOG_TRACE("Client: genNtpReq: cookiePlaceholder extension(s) created (amount: {}, size per placeholder: {})",
					m_clientState.numPlaceholder,
					placeholderSize);
			}
		}
	}

	// create and prepare EF: NTS Authenticator and Encrypted Extension Fields
	NtpExtensionField &AuthAndEncExF = m_clientState.authAndEncExtField;
	ExtField::NtsAuthAndEncEF::format(AuthAndEncExF);
	m_clientState.nonce = getRandomNumber(NONCE_SIZE);


	// calculate NTS content size
	std::size_t ntsContentSize = 0;
	ntsContentSize += uniqIdentEF.getExtensionFieldLength();
	ntsContentSize += ntsCookie.getExtensionFieldLength();
	ntsContentSize += (m_clientState.placeholderExtField.getExtensionFieldLength() * m_clientState.numPlaceholder);
	ntsContentSize += ExtField::NtsAuthAndEncEF::calculateContentSize(m_clientState.negAeadAlgo, 0, NONCE_SIZE) + 4; // 4: EF header size


	LOG_TRACE("Client: genNtpReq: calculated sizes: additional NTS content: {} bytes",ntsContentSize);

	// success
	m_clientState.messageState = MessageState::init_complete;
	ntpPacketInfo.setErrorCode(0);
	ntpPacketInfo.setNtsContentLengthNeeded(ntsContentSize);
	return true;
}








bool NtsClientNtp::createRequest_update(NtpPacketInfoImpl & ntpPacketInfo)
{
	// set error codes
	ntpPacketInfo.setErrorCode(-1);

	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();
	ntpPacket.printCurrentNtp();
	LOG_TRACE("Client: genNtpReq: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Client: genNtpReq: number of NTP EFs: {}", extFieldStack.getStackSize());


	NtpExtensionField & uniqIdentEF = m_clientState.uidExtField;
	extFieldStack.addExtField(uniqIdentEF);
	LOG_TRACE("Client: genNtpReq: NTP EF (uniqueID) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
		uniqIdentEF.getFieldTypeVal(),
		uniqIdentEF.getValueLength(),
		getItemID(uniqIdentEF.getValue()),
		uniqIdentEF.getSerializedData().size());


	NtpExtensionField & ntsCookie = m_clientState.cookieExtField;
	extFieldStack.addExtField(ntsCookie);
	LOG_TRACE("Client: genNtpReq: NTP EF (ntsCookie) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
		ntsCookie.getFieldTypeVal(),
		ntsCookie.getValueLength(),
		getItemID(ntsCookie.getValue()),
		ntsCookie.getSerializedData().size());


	NtpExtensionField & placeholderExtField = m_clientState.placeholderExtField;
	for (std::size_t i = 1; i <= m_clientState.numPlaceholder; i++)
	{
		extFieldStack.addExtField(placeholderExtField);
		LOG_TRACE("Client: genNtpReq: NTP EF (placeholder) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
			placeholderExtField.getFieldTypeVal(),
			placeholderExtField.getValueLength(),
			getItemID(placeholderExtField.getValue()),
			placeholderExtField.getSerializedData().size());
	}

	//ntpPacket.printFinalNtp();
	if (ntpPacket.overwriteNtpPacket() == false)
	{
		return false;
	}

	ntpPacket.printCurrentNtp();

	// success
	m_clientState.messageState = MessageState::update_complete;
	ntpPacketInfo.setErrorCode(0);
	return true;
}




bool NtsClientNtp::createRequest_final(NtpPacketInfoImpl & ntpPacketInfo)
{
	// set error codes
	ntpPacketInfo.setErrorCode(-1);

	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();
	LOG_TRACE("Client: genNtpReq: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Client: genNtpReq: number of NTP EFs: {}", extFieldStack.getStackSize());


	// ==================================== TIME CRIT ====================================
	auto start = std::chrono::steady_clock::now();
	// -----------------------------------------------------------------------------------

	std::vector<unsigned char> associatedData = ntpPacket.getSerializedPackage();
	NtpExtensionField &AuthAndEncExF = m_clientState.authAndEncExtField;
	ExtField::NtsAuthAndEncEF::performAead(AuthAndEncExF, m_clientState.negAeadAlgo, m_clientState.C2SKey, m_clientState.nonce, associatedData);

	// AuthAndEncExF in NTP einbetten
	extFieldStack.addExtField(AuthAndEncExF);


	if (ntpPacket.overwriteNtpPacket() == true)
	{
		m_clientState.messageState = MessageState::final_complete;
		ntpPacketInfo.setErrorCode(0);
	}


	// -----------------------------------------------------------------------------------
	auto end = std::chrono::steady_clock::now();
	auto time = end - start;
	std::chrono::duration<double, std::micro> proccess_time_us = time;
	LOG_DEBUG("Client: genNtpReq: TimeRequest crit time: {} us", proccess_time_us.count());
	// ==================================== TIME CRIT ====================================

	LOG_TRACE("Client: genNtpReq: perform AEAD (serialized NTP ID: {}, serialized NTP size: {})",
		getItemID(associatedData),
		associatedData.size());

	LOG_TRACE("Client: genNtpReq: NTP EF (AEAD ExtF) added (type: {}, value size: {}, raw EF size: {})",
		AuthAndEncExF.getFieldTypeVal(),
		AuthAndEncExF.getValueLength(),
		AuthAndEncExF.getSerializedData().size());

	// ADVANCED TRACE OUT


	

	CONDITIONAL_TRACE
	(
		int numEFs = extFieldStack.getStackSize();
	std::vector<unsigned char> rawNtpPacket = ntpPacket.getSerializedPackage();
	LOG_TRACE("Client: genNtpReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		numEFs);


	for (int i = 1; i <= numEFs; i++)
	{
		LOG_TRACE("Client: genNtpReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			extFieldStack.getExtField(i).getFieldTypeVal(),
			extFieldStack.getExtField(i).getValueLength(),
			getItemID(extFieldStack.getExtField(i).getValue()));
	}
	LOG_TRACE("Client: genNtpReq: FINISHED");
	LOG_TRACE("");
	);
	// ADVANCED TRACE OUT

	ntpPacket.printCurrentNtp();
	return true;
}


















