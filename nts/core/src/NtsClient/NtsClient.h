/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <list>
#include "NtsClientSession.h"
#include <string>
//#include ".././NtpPacket.h"

struct NtpPacketInfoImpl;
struct NtsSettings;

class NtsClient
{
public:
	NtsClient() = delete;
	NtsClient(NtsSettings const& settings);
	~NtsClient() = default;

	void processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createRequest(NtpPacketInfoImpl& ntpPacketInfo, int stage);

private:
	NtsClientSession & getSession(std::string const & keServerIp);

	std::list <NtsClientSession> m_clientSessionList;
	NtsSettings const & m_settings;

//	NtpPacket ntp;
};

