#include "./BaseConfig.h" // muss an erster stelle stehen (avoid warning: -D_WIN32_WINNT=0x0501)
#include "NtsUnicastServiceImpl.h"
#include "NtpPacketInfoImpl.h"
#include "./Logger/Logger.h"
#include <boost/filesystem.hpp>
#include "NtsConfig.h"


NtsUnicastServiceImpl::NtsUnicastServiceImpl()
	:
	m_ntsClient(m_settings),
	m_ntsServer(m_settings),
	m_ntsMode(NtsMode::not_set),
	m_ntsState(NtsState::not_init)
{
}


void NtsUnicastServiceImpl::initialize(std::string const & globalConfigFile)
{
	try
	{
		m_configFile = globalConfigFile;
		m_settings = readConfigFile(globalConfigFile);
		m_ntsMode = m_settings.general.ntsServiceMode;
		configureLogger(m_settings);

		if (m_ntsMode == NtsMode::client)
		{
			LOG_INFO("NTS mode: client");
			// keine init funktion n�tig
		}
		else if (m_ntsMode == NtsMode::server)
		{
			LOG_INFO("NTS mode: server");
			m_ntsServer.initialization();
		}
		else
		{
			LOG_DEBUG("NTS mode: not defined!");
		}

		m_ntsState = NtsState::init;
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}


void NtsUnicastServiceImpl::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingReceivedNtpMessage: NTS is not init");
		}

		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.processAndVerify(ntpPacketInfo);
		}
		else if (m_ntsMode == NtsMode::server)
		{
			m_ntsServer.processAndVerify(ntpPacketInfo);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingReceivedNtpMessage: invalid nts mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}




void NtsUnicastServiceImpl::createMessage_init(NtpPacketInfoImpl & ntpPacketInfo)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not init");
		}
		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.createRequest(ntpPacketInfo, 1);
		}
		else if (m_ntsMode == NtsMode::server)
		{
			m_ntsServer.createRequest(ntpPacketInfo, 1);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not in client mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		// TODO: setError in ntpPacketInfo
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}


void NtsUnicastServiceImpl::createMessage_update(NtpPacketInfoImpl & ntpPacketInfo)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not init");
		}
		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.createRequest(ntpPacketInfo, 2);
		}
		else if (m_ntsMode == NtsMode::server)
		{
			m_ntsServer.createRequest(ntpPacketInfo, 2);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not in client mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		// TODO: setError in ntpPacketInfo
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}


void NtsUnicastServiceImpl::createMessage_final(NtpPacketInfoImpl & ntpPacketInfo)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not init");
		}
		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.createRequest(ntpPacketInfo, 3);
		}
		else if (m_ntsMode == NtsMode::server)
		{
			m_ntsServer.createRequest(ntpPacketInfo, 3);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not in client mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		// TODO: setError in ntpPacketInfo
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}


bool NtsUnicastServiceImpl::isInit()
{
	if (m_ntsState == NtsState::init)
	{ 
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsUnicastServiceImpl::isClientMode()
{
	if (m_ntsMode == NtsMode::client)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsUnicastServiceImpl::isServerMode()
{
	if (m_ntsMode == NtsMode::server)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void NtsUnicastServiceImpl::showInfos()
{
	std::string current_path = boost::filesystem::path(boost::filesystem::current_path()).string();

#ifdef _DEBUG
	LOG_INFO("NTS version:          NTS (draft-ietf-ntp-using-nts-for-ntp-17) v{} ({}) - debug build", NTS_VERSION, NTS_COMPILE_TIME);
#else
	LOG_INFO("NTS version:          NTS (draft-ietf-ntp-using-nts-for-ntp-17) v{} ({}) - release build", NTS_VERSION, NTS_COMPILE_TIME);
#endif
	LOG_DEBUG("work directory:       {}", current_path);
	LOG_DEBUG("nts config file:      {}", m_configFile);
	LOG_DEBUG("OpenSSL version:      {}", OpenSSL_version(OPENSSL_VERSION));
	LOG_DEBUG("Boost version:        {}.{}.{}", (BOOST_VERSION / 100000), (BOOST_VERSION / 100 % 1000), (BOOST_VERSION % 100));
	LOG_DEBUG("libaes_siv version:   1.0");
	LOG_DEBUG("spdlog version:       {}.{}.{}", SPDLOG_VER_MAJOR, SPDLOG_VER_MINOR, SPDLOG_VER_PATCH);
	LOG_DEBUG("FMT version:          {}.{}.{}\n", (FMT_VERSION / 10000), (FMT_VERSION / 100 % 100), (FMT_VERSION % 100));
}
