/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <string>
#include <memory>
#include <cstddef>
#include "NtpPacket.h"
#include "NtsServer/ServerState.h"


struct NtpPacketInfoImpl
{

public:
	NtpPacketInfoImpl();
	~NtpPacketInfoImpl() = default;

	void setServer(std::string const& ntsKeServerIp, std::string const& ntpTimeServerIp, unsigned short ntpTimeServerPort);

	std::string const & getNtsKeServerIp() const;
	std::string const & getNtpTimeServerIp() const;
	unsigned short getNtpTimeServerPort() const;

	void setErrorCode(int ec);
	int getErrorCode() const;

	void setCurrentNtsContentLength(std::size_t length);
	std::size_t getCurrentNtsContentLength() const;

	void setNtsContentLengthNeeded(std::size_t length);
	std::size_t getNtsContentLengthNeeded() const;

	void setNtpPacket(unsigned char* ntpPacket, std::size_t packetLen, std::size_t bufferSize, bool rescan);
	NtpPacket & getNtpPacket();
	ServerState & getServerState();




private:

	std::string m_ntsKeServerIp;
	std::string m_ntpTimeServerIp;
	unsigned short m_ntpTimeServerPort;

	int m_errorCode;
	std::size_t m_currentNtsContentLength; // derzeitige Gr��e der enthaltenen NTS-Inhalte im NTP-Paket
	std::size_t m_ntsContentLengthNeeded;  // ben�tigte Gr��e zum sichern des NTP-Pakets

	NtpPacket m_ntpPacket;
	ServerState m_serverState;


};

