/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
//#include <deque>
//#include ".././NtpPacket.h"
#include "../NtpExtensionFields/NtpExtensionField.h"
#include "../NtpExtensionFields/NtpExtFieldStack.h"
#include <cstddef>

//enum class NextProtocol : unsigned short;
enum class AeadAlgorithm : unsigned short;
enum class MessageState;




class ServerState
{
public:

	ServerState() = default;
	~ServerState() = default;

	void clearMessageState();

	std::size_t ntpRequestMessageLength{ 0 };
	std::vector<unsigned char> C2SKey;
	std::vector<unsigned char> S2CKey;
	AeadAlgorithm negAeadAlgo;
	std::size_t numPlaceholder{ 0 };
	unsigned short placeholderSize{ 0 };



	NtpExtFieldStack receivedExtFieldStack;
	std::vector<unsigned char> decryptedExtFields;
	MessageState messageState;



	//// nts key exchange
	//std::vector<NextProtocol> supportedNextProt;
	//std::vector<AeadAlgorithm> supportedAeadAlgos;
	//std::vector<NextProtocol> negNextProt;
	


	std::vector<unsigned char> cookieStackStream;

	// extension fields
	NtpExtensionField uidExtField;
	NtpExtensionField authAndEncExtField;
	std::vector<unsigned char> nonce;
};

