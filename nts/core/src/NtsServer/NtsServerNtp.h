/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>

struct NtpPacketInfoImpl;
class NtpPacket;
class MasterKey;
class NtpExtensionField;
struct NtsSettings;
class ServerState;

enum class AeadAlgorithm : unsigned short;



class NtsServerNtp
{
public:
	NtsServerNtp() = delete;
	NtsServerNtp(NtsSettings const& settings);
	~NtsServerNtp() = default;


	bool processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createRequest(NtpPacketInfoImpl &ntpPacketInfo, int stage);
	void setMasterKey(MasterKey *masterKey);

private:

	bool createRequest_init(NtpPacketInfoImpl &ntpPacketInfo); // create and prepare EFs and calc memory usage
	bool createRequest_update(NtpPacketInfoImpl &ntpPacketInfo); // write EFs (without AEAD)
	bool createRequest_final(NtpPacketInfoImpl &ntpPacketInfo); // securing NTP packet


	bool decryptAndParseCookie(std::vector<unsigned char> const &encryptedCookie, ServerState & serverState);
	bool verifyNtpMessage(NtpPacket &ntpPacket, NtpExtensionField const &aeadExtField, ServerState & serverState);
	std::vector<unsigned char> createCookie(ServerState & serverState);


	AeadAlgorithm getServerAeadAlgo() const;
	std::vector<unsigned char> getServerAeadKey(unsigned short keyId) const;
	unsigned short getServerAeadKeyId() const;


	MasterKey *m_masterKey; // TODO: pointer?  dann wenigsten allozieren oder was?   ...oder nullptr?
	NtsSettings const & m_settings; // ben�tigt?
};

