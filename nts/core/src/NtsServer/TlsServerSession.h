/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "NtsServerKe.h"
#include <vector>
#include <cstddef>

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

class TlsServerSession
{
public:
	TlsServerSession (boost::asio::io_service& io_service, boost::asio::ssl::context& context, NtsServerKe ntsServerKe);


	ssl_socket::lowest_layer_type& getSocket ();
	void startup ();
	void handshakeHandler (const boost::system::error_code& error);
	void readHandler (const boost::system::error_code& error, std::size_t bytesTransferred);
	void writeHandler (const boost::system::error_code& error);
	void shutdownHandler(const boost::system::error_code& error);

	void setFinished(bool state);
	bool isFinished() const;


	static int alpnSelectCallback(SSL *ssl,
								  const unsigned char **out,
								  unsigned char *outlen,
								  const unsigned char *in,
								  unsigned int inlen,
								  void *arg);

private:
	bool m_isFinished;
	ssl_socket m_socket;
	boost::asio::ssl::context &m_context;
	std::vector<unsigned char> m_dataBuffer;
	std::vector<unsigned char> m_alpnList;
	NtsServerKe m_ntsServerKe;
};
