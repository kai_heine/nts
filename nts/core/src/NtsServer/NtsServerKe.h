/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include <mutex>
#include <openssl/ossl_typ.h>


struct NtsSettings;
class MasterKey;
class Record;
class RecordStack;
enum class NextProtocol : unsigned short;
enum class AeadAlgorithm : unsigned short;



class NtsServerKe
{
  public:
	NtsServerKe() = delete;
	NtsServerKe(NtsSettings const &settings);
	~NtsServerKe() = default;

	// ret response message
	std::vector<unsigned char> processTlsRequest(std::vector<unsigned char> const &tlsAppDataPayload,
	                                             SSL &ssl) const;
	void setMasterKey(MasterKey *masterKey);

  private:
	bool processNextProtNeg(RecordStack recStack, Record &recordOut) const;
	bool processAeadAlgoNeg(RecordStack recStack, Record &recordOut) const;
	// ret true on success; false: server kann die Prots nicht sprechen
	bool selectNextProtNeg(std::vector<unsigned short> &nextProtNegList) const;
	// ret true on success; false: server kann die Prots nicht sprechen
	bool selectAeadAlgorithm(std::vector<unsigned short> &aeadAlgoList) const;
	std::vector<unsigned char> createCookie(AeadAlgorithm negAeadAlgo, std::vector<unsigned char> S2CKey,
	                                        std::vector<unsigned char> C2SKey) const;
	bool isSupported(NextProtocol nextProt) const;
	bool isSupported(AeadAlgorithm AeadAlgo) const;
	std::vector<unsigned char> extractTLSKey(SSL &ssl, std::string tlsExporterLabel, NextProtocol nextProt,
	                                         AeadAlgorithm aeadAlgo, unsigned char mode) const;
	// der Master AEAD Algo. des Servers
	AeadAlgorithm getServerAeadAlgo() const;
	// liefert den Key zur passenden KeyId
	std::vector<unsigned char> getServerAeadKey(unsigned short keyId) const;
	// liefert die aktuelle ID des Masterkeys
	unsigned short getServerAeadKeyId() const;



	NtsSettings const &m_settings;
	MasterKey *m_masterKey; // TODO pointer?
	std::vector<AeadAlgorithm> const &m_supportedAeadAlgos;
	std::vector<NextProtocol> const &m_supportedNextProt;
	static std::mutex m_mutex;
};
