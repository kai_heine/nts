#include "ServerState.h"
#include "../NtsConfig.h"



void ServerState::clearMessageState()
{
	//negNextProt.clear();
	ntpRequestMessageLength = 0;
	negAeadAlgo = AeadAlgorithm::NOT_DEFINED;
	C2SKey.clear();
	S2CKey.clear();
	cookieStackStream.clear();
	placeholderSize = 0;
	numPlaceholder = 0;
	receivedExtFieldStack.clear();
	decryptedExtFields.clear();
	uidExtField.clear();
	authAndEncExtField.clear();
	nonce.clear();
	messageState = MessageState::uninitialized;
}
