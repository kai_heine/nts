#include "../BaseConfig.h" // muss an erster stelle stehen (avoid warning: -D_WIN32_WINNT=0x0501)
#include "NtsServer.h"
#include "../cryptoFunctions.h"
#include "../Logger/Logger.h"
#include "../NtsSettings.h"


NtsServer::NtsServer(NtsSettings const& settings)
	:
	m_ioService(),
	m_tlsThread(),
	m_ntsKeyEstablishment(settings),
	m_masterKey(),
	m_ntpCommunication(settings),
	m_settings(settings)
{
}

NtsServer::~NtsServer()
{
	stopTlsServer();
}


void NtsServer::initialization() // todo: 'initialize()'
{
	// generate MasterKey
	m_masterKey.setRotateInterval(m_settings.server.keyRotationIntervalSeconds);
	m_masterKey.setMaxRotations(m_settings.server.keyRotationHistory);
	m_masterKey.setKeyLengthInBytes(determineKeySize(m_settings.server.aeadAlgoForMasterKey));

	HKDF hkdf;
	hkdf.setMessageDigestAlgorithm(m_settings.server.mdAlgoForMasterKeyHkdf);
	hkdf.setKeyOutputLength(determineKeySize(m_settings.server.aeadAlgoForMasterKey));
	m_masterKey.setHkdf(hkdf);

	m_masterKey.forceRotate();
	m_masterKey.startAutoRotate();


	m_ntsKeyEstablishment.setMasterKey(&m_masterKey);
	m_ntpCommunication.setMasterKey(&m_masterKey);

	// start async TLS server
	if (startTlsServer(m_settings.server.defaultKeServerTcpPort) == false)
	{
		LOG_WARN("NtsServer::initialization(): TLS Server runs already");
	}
}


void NtsServer::shutdown()
{
	m_masterKey.stopAutoRotate();
	m_masterKey.clearAllKeys();
	stopTlsServer();
}



void NtsServer::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{
	m_ntpCommunication.processAndVerify(ntpPacketInfo);
}


void NtsServer::createRequest(NtpPacketInfoImpl & ntpPacketInfo, int stage)
{
	m_ntpCommunication.createRequest(ntpPacketInfo, stage);
}



bool NtsServer::startTlsServer(int port)
{
	if (m_tlsThread)
	{
		return false;
	}

	try
	{
		m_tlsServer.reset(new TlsServer(m_ioService, port, m_ntsKeyEstablishment, m_settings));
		m_tlsThread.reset(new boost::thread(boost::bind(&boost::asio::io_service::run, &m_ioService)));
	}
	catch (...)
	{
		LOG_ERROR("unable to start the TLS server");
		throw;
	}
	return true;
}


bool NtsServer::stopTlsServer()
{
	if (!m_tlsThread)
	{
		return false;
	}

	m_ioService.stop();
	m_tlsThread->join();
	m_ioService.reset();
	m_tlsThread.reset();
	return true;
}


