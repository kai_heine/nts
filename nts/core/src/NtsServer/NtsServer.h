/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include "NtsServerNtp.h"
#include "./TlsServer.h"
#include "../MasterKey.h"


struct NtpPacketInfoImpl;
struct NtpSettings;



class NtsServer
{
public:
	NtsServer() = delete;
	NtsServer(NtsSettings const& settings);
	~NtsServer();
	NtsServer(const NtsServer&) = delete; // copy constructor
	NtsServer(NtsServer&&) = delete; // move constructor
	NtsServer& operator=(const NtsServer&) = delete; // copy assignment
	NtsServer& operator=(NtsServer &&) = delete; // move assignment 



	void initialization();
	void shutdown();

	void processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createRequest(NtpPacketInfoImpl& ntpPacketInfo, int stage);





private:
	bool startTlsServer(int port);	// ret: false: tls already running  /  true: tls started
	bool stopTlsServer();			// ret: false: tls already stopped  /  true: tls stopped

	boost::asio::io_service m_ioService;
	boost::scoped_ptr<boost::thread> m_tlsThread;
	std::unique_ptr<TlsServer> m_tlsServer;
	NtsServerKe m_ntsKeyEstablishment;
	MasterKey m_masterKey;
	NtsServerNtp m_ntpCommunication;
	NtsSettings const & m_settings;
};

