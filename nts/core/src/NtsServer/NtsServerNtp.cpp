#include "NtsServerNtp.h"
#include "../AEAD.h"
#include "../Logger/Logger.h"
#include "../cryptoFunctions.h"
#include "../NtpExtensionFields/NtpExtFieldStack.h"
#include "../NtpExtensionFields/NtpExtFieldStackVerification.h"
#include "../NtpExtensionFields/ExtF_NtsCookie.h"
#include "../NtpExtensionFields/ExtF_NtsCookiePlaceholder.h"
#include "../NtpExtensionFields/ExtF_UniqueIdentifier.h"
#include "../NtpExtensionFields/ExtF_NtsAuthAndEncEF.h"
#include "../Cookie/Cookie.h"
#include "../Cookie/CookieContent.h"
#include ".././NtpPacket.h"
#include "../NtpPacketInfoImpl.h"
#include "../MasterKey.h"
#include "../NtsSettings.h"
#include "../NtpExtensionFields/NtpExtensionField.h"



NtsServerNtp::NtsServerNtp(NtsSettings const& settings)
	:
	m_settings(settings)
{
}


bool NtsServerNtp::processAndVerify(NtpPacketInfoImpl & ntpPacketInfo)
{
	LOG_TRACE("");
	LOG_TRACE("Server: verifyReq: STARTED");
	//LOG_TRACE("Server: genNtpReq: client IP address: {}", ntpPacketInfo.senderIP);


	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();
	ServerState &serverState = ntpPacketInfo.getServerState();

	// set error codes
	serverState.clearMessageState();
	ntpPacketInfo.setErrorCode(-1);
	serverState.messageState = MessageState::uninitialized;

	std::size_t existingContentSize = 0;


	serverState.ntpRequestMessageLength = ntpPacket.getNtpPacketLength();
	ntpPacket.printCurrentNtp();
	LOG_TRACE("Server: verifyReq: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Server: verifyReq: number of NTP EFs: {}", extFieldStack.getStackSize());


	// ADVANCED TRACE OUT
	int numEFs = extFieldStack.getStackSize();
	std::vector<unsigned char> rawNtpPacket = ntpPacket.getSerializedPackage();
	LOG_TRACE("Server: verifyReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}", rawNtpPacket.size(), getItemID(rawNtpPacket), numEFs);

	for (int i = 1; i <= numEFs; i++)
	{
		LOG_TRACE("Server: verifyReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			extFieldStack.getExtField(i).getFieldTypeVal(),
			extFieldStack.getExtField(i).getValueLength(),
			getItemID(extFieldStack.getExtField(i).getValue())
		);
	}
	// ADVANCED TRACE OUT

	LOG_TRACE("Server: verifyReq: Phase 1 of 2 - verify request");


	// Erweiterungsfelder extrahieren    
	int numReceivedExtFields = extFieldStack.getStackSize();
	NtpExtFieldStack & receivedExtFieldStack = serverState.receivedExtFieldStack;
	if (numReceivedExtFields == 0)
	{
		LOG_ERROR("Server: verifyReq: empfangenes NTP beinhaltet keine EFs");
		return false;
	}
	for (int i = 1; i <= numReceivedExtFields; i++)
	{
		NtpExtensionField ntpExtF;
		ntpExtF.setFieldType(extFieldStack.getExtField(i).getFieldTypeVal());
		ntpExtF.setValue(extFieldStack.getExtField(i).getValue());
		receivedExtFieldStack.addExtField(ntpExtF);  //TODO: hier wäre const& sinnvoll --> NTP/NTS interface anpassen?
	}


	// check record stack (is it NTS conform?)
	if (NtpExtFieldStackVerification().isStackValid(receivedExtFieldStack, NtpMode::Client_mode, true) == false)
	{
		LOG_ERROR("Server: verifyReq: wrong NTP EF constellation");
		return false;
	}


	// Cookie extrahieren, entschlüsseln und parsen
	unsigned int ntsCookieIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Cookie);
	if (ntsCookieIndex == 0)
	{
		LOG_ERROR("Server: genNtpReq: ntsCookie not found");
		return false;
	}
	const NtpExtensionField &extractedNtsCookie = receivedExtFieldStack.getExtField(ntsCookieIndex);
	existingContentSize += extractedNtsCookie.getExtensionFieldLength();
	if (ExtField::NtsCookie::isValid(extractedNtsCookie) == false)
	{
		LOG_ERROR("Server: verifyReq: extracted raw Cookie is invalid");
		return false;
	}
	const std::vector<unsigned char> &rawCookie = ExtField::NtsCookie::getCookie(extractedNtsCookie);
	if (decryptAndParseCookie(rawCookie, serverState) == false)
	{
		LOG_ERROR("Server: verifyReq: decryptAndParseCookie failed");
		return false;
	}
	LOG_TRACE("Server: verifyReq: ntsCookie extracted (cookie ID: {}, size: {})",
		getItemID(ExtField::NtsCookie::getCookie(extractedNtsCookie)),
		ExtField::NtsCookie::getCookieSize(extractedNtsCookie));



	// Cookie size == placeholder size?
	std::size_t numPlaceholder = 0;
	unsigned int cookieSize = ExtField::NtsCookie::getCookieSize(extractedNtsCookie);
	for (unsigned int index = 1; index <= receivedExtFieldStack.getStackSize(); index++)
	{
		const NtpExtensionField &extField = receivedExtFieldStack.getExtField(index);
		if (extField.getFieldType() == NtpExtFieldType::NTS_Cookie_Placeholder)
		{
			existingContentSize += extField.getExtensionFieldLength();
			numPlaceholder++;
			unsigned int placeholderSize = ExtField::NtsCookiePlaceholder::getPlaceholderSize(extField);
			LOG_TRACE("Server: verifyReq: cookie placeholder found (EF index: {}, placeholder size: {}", index - 1, placeholderSize);
			if (placeholderSize != cookieSize)
			{
				LOG_ERROR("Server: verifyReq: placeholderSize mismatch");
				return false;
			}
			else
			{
				serverState.placeholderSize = static_cast<unsigned short>(placeholderSize);
			}
		}
	}
	serverState.numPlaceholder = numPlaceholder;



	// Integritätsprüfung der empfangenen Nachricht
	unsigned int aeadExtFieldIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
	if (aeadExtFieldIndex == 0)
	{
		LOG_ERROR("Server: genNtpReq: AuthAndEncExtField not found");
		return false;
	}
	const NtpExtensionField &extractedAeadEF = receivedExtFieldStack.getExtField(aeadExtFieldIndex);
	existingContentSize += extractedAeadEF.getExtensionFieldLength();
	if (verifyNtpMessage(ntpPacket, extractedAeadEF, serverState) == false)
	{
		LOG_ERROR("Server: verifyReq: Integrity check: FAILED");
		return false;
	}
	else
	{
		LOG_TRACE("Server: verifyReq: Integrity check: SUCCESS");
	}


	// FOR TRACE
	rawNtpPacket = ntpPacket.getSerializedPackage();
	LOG_TRACE("Server: verifyReq: NTP Packet (after AEAD --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		receivedExtFieldStack.getStackSize());


	// ggf. verschlüsselte EF verarbeiten
	if (serverState.decryptedExtFields.size() != 0)
	{
		// mach irgendwas
		// todo: what we do if cookies available
		LOG_INFO("Server: verifyReq: encrypted extension fields found");
	}

	
	unsigned int UIDindex = receivedExtFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	if (UIDindex == 0)
	{
		LOG_ERROR("Server: genNtpReq: UniqueIdentifierEf not found");
		return false;
	}
	const NtpExtensionField &extractedUID = receivedExtFieldStack.getExtField(UIDindex);
	existingContentSize += extractedUID.getExtensionFieldLength();
	LOG_TRACE("Server: genNtpReq: NTS content size (received nts message): {} bytes", existingContentSize);



	// success
	serverState.messageState = MessageState::verified;
	ntpPacketInfo.setErrorCode(0);
	ntpPacketInfo.setCurrentNtsContentLength(existingContentSize);
	return true;
}





void NtsServerNtp::createRequest(NtpPacketInfoImpl & ntpPacketInfo, int stage)
{
	if (stage == 1)
	{

		LOG_TRACE("");
		LOG_TRACE("Server: genNtpReq: STARTED");
		LOG_TRACE("Server: genNtpReq: STAGE 1");

		if (ntpPacketInfo.getServerState().messageState != MessageState::verified)
		{
			LOG_WARN("Server: genNtpReq: invalid message state");
			ntpPacketInfo.setErrorCode(-1);
		}
		else
		{
			createRequest_init(ntpPacketInfo);
		}
	}
	else if (stage == 2)
	{
		LOG_TRACE("Server: genNtpReq: STAGE 2");
		if ((ntpPacketInfo.getServerState().messageState == MessageState::init_complete) 
			|| (ntpPacketInfo.getServerState().messageState == MessageState::update_complete)
			|| (ntpPacketInfo.getServerState().messageState == MessageState::final_complete))
		{
			createRequest_update(ntpPacketInfo);
		}
		else
		{
			LOG_WARN("Server: genNtpReq: invalid message state");
			ntpPacketInfo.setErrorCode(-1);
		}
	}
	else if (stage == 3)
	{
		LOG_TRACE("Server: genNtpReq: STAGE 3");
		if ((ntpPacketInfo.getServerState().messageState == MessageState::update_complete)
			|| (ntpPacketInfo.getServerState().messageState == MessageState::final_complete))
		{
			createRequest_final(ntpPacketInfo);
		}
		else
		{
			LOG_WARN("Server: genNtpReq: invalid message state");
			ntpPacketInfo.setErrorCode(-1);
		}
	}
	else
	{
		LOG_WARN("Server: genNtpReq: invalid message state");
	}
}






bool NtsServerNtp::createRequest_init(NtpPacketInfoImpl & ntpPacketInfo)
{
	// set error codes
	ntpPacketInfo.setErrorCode(-1);

	ServerState &serverState = ntpPacketInfo.getServerState();
	serverState.messageState = MessageState::verified;
	NtpExtFieldStack & receivedExtFieldStack = serverState.receivedExtFieldStack;

	

	// create EF: Unique Identifier
	int index = receivedExtFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	if (index == 0)
	{
		LOG_ERROR("Server: genNtpReq: uniqIdentEF not found");
		return false;
	}
	const NtpExtensionField &uniqIdentEF = receivedExtFieldStack.getExtField(index);
	if (ExtField::UniqueIdentifier::isValid(uniqIdentEF) == false) // Todo: dies in der ExtFieldVerification machen
	{
		LOG_WARN("Server: genNtpReq: uniqIdentEF invalid");
	}
	
	const std::vector<unsigned char> &uniqueIdVal = ExtField::UniqueIdentifier::getUniqueId(uniqIdentEF);

	NtpExtensionField & newUniqIdentEF = serverState.uidExtField;
	ExtField::UniqueIdentifier::format(newUniqIdentEF);
	ExtField::UniqueIdentifier::setUniqueId(newUniqIdentEF, uniqueIdVal);


	// create EF: NTS Cookie(s) 
	std::vector<NtpExtensionField> ntsCookieStack;
	int numPlaceholder = static_cast<int>(serverState.numPlaceholder);
	int numNewCookies = 1 + numPlaceholder;
	if (numNewCookies < 0)
	{
		LOG_ERROR("Server: genNtpReq: numNewCookies is <0");
		return false;
	}
	else if (numNewCookies > 0)
	{
		for (int i = 1; i <= numNewCookies; i++)
		{
			NtpExtensionField cookie;
			ExtField::NtsCookie::format(cookie);
			ExtField::NtsCookie::setCookie(cookie, createCookie(serverState));
			ntsCookieStack.push_back(cookie);
		}
	}

	// generate raw cookie extension field stream
	std::vector<unsigned char> & plainExtensionStream = serverState.cookieStackStream;
	for (auto cookie : ntsCookieStack)
	{
		const std::vector<unsigned char> &stream = cookie.getSerializedData();
		plainExtensionStream.insert(plainExtensionStream.end(), stream.begin(), stream.end());
		LOG_TRACE("Server: genNtpReq: NTP EF --> AuthAndEncExt EF: (ntsCookie) added (type: {}, value size: {}, value ID: {})",
			cookie.getFieldTypeVal(),
			cookie.getValueLength(),
			getItemID(cookie.getValue()));
	}
	

	// create EF: NTS Authenticator and Encrypted Extension Fields
	NtpExtensionField & AuthAndEncExF = serverState.authAndEncExtField;
	ExtField::NtsAuthAndEncEF::format(AuthAndEncExF);
	serverState.nonce = getRandomNumber(NONCE_SIZE);
	
	

	// calculate NTS content size
	std::size_t ntsContentSize = 0;
	ntsContentSize += uniqIdentEF.getExtensionFieldLength();
	ntsContentSize += ExtField::NtsAuthAndEncEF::calculateContentSize(serverState.negAeadAlgo, plainExtensionStream.size(), NONCE_SIZE) + 4; // 4: EF header size
	LOG_TRACE("Server: genNtpReq: calculated sizes: additional NTS content: {} bytes", ntsContentSize);


	// success
	serverState.messageState = MessageState::init_complete;
	ntpPacketInfo.setErrorCode(0);
	ntpPacketInfo.setNtsContentLengthNeeded(ntsContentSize);
	return true;
}


bool NtsServerNtp::createRequest_update(NtpPacketInfoImpl & ntpPacketInfo)
{
	// set error codes
	ntpPacketInfo.setErrorCode(-1);
	ServerState &serverState = ntpPacketInfo.getServerState();

	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();

	ntpPacket.printCurrentNtp();
	extFieldStack.clear();
	if (ntpPacket.overwriteNtpPacket() == false)
	{
		return false;
	}


	LOG_TRACE("Server: genNtpReq: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Server: genNtpReq: number of NTP EFs: {}", extFieldStack.getStackSize());


	NtpExtensionField & uniqIdentEF = serverState.uidExtField;
	extFieldStack.addExtField(uniqIdentEF);
	LOG_TRACE("Server: genNtpReq: NTP EF (uniqueID) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
		uniqIdentEF.getFieldTypeVal(),
		uniqIdentEF.getValueLength(),
		getItemID(uniqIdentEF.getValue()),
		uniqIdentEF.getSerializedData().size());


	ntpPacket.printCurrentNtp();
	if (ntpPacket.overwriteNtpPacket() == false)
	{
		return false;
	}

	ntpPacket.printCurrentNtp();

	// success
	serverState.messageState = MessageState::update_complete;
	ntpPacketInfo.setErrorCode(0);
	return true;
}



bool NtsServerNtp::createRequest_final(NtpPacketInfoImpl & ntpPacketInfo)
{

	// set error codes
	ntpPacketInfo.setErrorCode(-1);
	ServerState &serverState = ntpPacketInfo.getServerState();

	NtpPacket &ntpPacket = ntpPacketInfo.getNtpPacket();
	NtpExtFieldStack & extFieldStack = ntpPacket.getExtFieldStack();
	LOG_TRACE("Server: genNtpReq: ntp isValid: {}", ntpPacket.isValid());
	LOG_TRACE("Server: genNtpReq: number of NTP EFs: {}", extFieldStack.getStackSize());


	// ==================================== TIME CRIT ====================================
	auto start = std::chrono::steady_clock::now();
	// -----------------------------------------------------------------------------------

	std::vector<unsigned char> associatedData = ntpPacket.getSerializedPackage();
	NtpExtensionField &AuthAndEncExF = serverState.authAndEncExtField;
	ExtField::NtsAuthAndEncEF::performAead(AuthAndEncExF, serverState.negAeadAlgo, serverState.S2CKey, serverState.cookieStackStream, serverState.nonce, associatedData);

	// AuthAndEncExF in NTP einbetten
	extFieldStack.addExtField(AuthAndEncExF);

	if (ntpPacket.overwriteNtpPacket() == true)
	{
		serverState.messageState = MessageState::final_complete;
		ntpPacketInfo.setErrorCode(0);
	}

	// -----------------------------------------------------------------------------------
	auto end = std::chrono::steady_clock::now();
	auto time = end - start;
	std::chrono::duration<double, std::micro> proccess_time_us = time;
	LOG_DEBUG("Server: genNtpReq: TimeResponse crit time: {} us", proccess_time_us.count());
	// ==================================== TIME CRIT ====================================

	LOG_TRACE("Server: genNtpReq: perform AEAD (serialized NTP ID: {}, serialized NTP size: {})",
		getItemID(associatedData),
		associatedData.size());

	LOG_TRACE("Server: genNtpReq: NTP EF (AEAD ExtF) added (type: {}, value size: {})",
		AuthAndEncExF.getFieldTypeVal(),
		AuthAndEncExF.getValueLength());


	CONDITIONAL_TRACE(
		int numEFs = extFieldStack.getStackSize();
		std::vector<unsigned char> rawNtpPacket = ntpPacket.getSerializedPackage();
		LOG_TRACE("Server: genNtpReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
			rawNtpPacket.size(),
			getItemID(rawNtpPacket),
			numEFs);

		for (int i = 1; i <= numEFs; i++)
		{
			LOG_TRACE("Server: genNtpReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
				i,
				extFieldStack.getExtField(i).getFieldTypeVal(),
				extFieldStack.getExtField(i).getValueLength(),
				getItemID(extFieldStack.getExtField(i).getValue()));
		}
		LOG_TRACE("Server: genNtpReq: FINISHED");
		LOG_TRACE("");
	);


	ntpPacket.printCurrentNtp();
	return true;
}






void NtsServerNtp::setMasterKey(MasterKey * masterKey)
{
	this->m_masterKey = masterKey;
}




bool NtsServerNtp::decryptAndParseCookie(std::vector<unsigned char> const &encryptedCookie, ServerState & serverState)
{
	bool status = true; 

	// check cookie
	Cookie cookie(encryptedCookie, NONCE_SIZE);
	if (cookie.isValid() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): cookie is invalid");
		status = false;
	}

	// parse cookie
	unsigned short keyID = cookie.getKeyId();
	const std::vector<unsigned char> &nonce = cookie.getNonce();
	const std::vector<unsigned char> &encContent = cookie.getEncCookieContent();

	// get Server AEAD key
	const std::vector<unsigned char> &serverAeadKey = getServerAeadKey(keyID);
	AeadAlgorithm serverAeadAlgo = getServerAeadAlgo();


	// decrypt cookie
	AEAD aead;
	aead.setAeadAlgorithm(serverAeadAlgo);
	aead.setKey(serverAeadKey);
	aead.setCiphertext(encContent);
	aead.setNonce(nonce);
	if (aead.decrypt() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): decryption failed");
		status = false;
	}

	// parse decrypted cookie
	CookieContent cookieContent(aead.getPlaintext());
	if (cookieContent.isValid() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): decrypted cookie is invalid");
		status = false;
	}

	// save extracted values
	serverState.negAeadAlgo = cookieContent.getAeadAlgorithm();
	serverState.C2SKey = cookieContent.getC2SKey();
	serverState.S2CKey = cookieContent.getS2CKey();

	return status;
}



bool NtsServerNtp::verifyNtpMessage(NtpPacket &ntpPacket, NtpExtensionField const &aeadExtField, ServerState & serverState)
{
	bool status = true;

	if (ExtField::NtsAuthAndEncEF::isValid(aeadExtField) == false)
	{
		LOG_ERROR("NtsServerNtp::verifyNtpMessage(): aeadExtField is invalid");
		status = false;
	}

	std::vector<unsigned char> nonce = ExtField::NtsAuthAndEncEF::getNonce(aeadExtField);
	std::vector<unsigned char> encryptedContent = ExtField::NtsAuthAndEncEF::getCiphertext(aeadExtField);

	// ADVANCED TRACE OUT
	int extFieldCount = ntpPacket.getExtFieldStack().getStackSize();
	for (int index = 1; index <= extFieldCount; index++)
	{
		if (ntpPacket.getExtFieldStack().getExtField(index).getFieldType() == NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields)
		{
			ntpPacket.getExtFieldStack().delExtField(index); // TODO: Ändern
			break;
		}
	}

	// check integrity and encrypt
	AEAD aead;
	aead.setAeadAlgorithm(serverState.negAeadAlgo);
	aead.setKey(serverState.C2SKey);
	aead.setCiphertext(encryptedContent);
	aead.setNonce(nonce);
	aead.setAssociatedData(ntpPacket.getSerializedPackage());



	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  AEAD algorithm:      {}", getAeadAlgorithmStr(serverState.negAeadAlgo));

	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  S2C key ID:          {}", getItemID(serverState.S2CKey));
	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  S2C key size:        {}", serverState.S2CKey.size());

	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  C2S key ID:          {}", getItemID(serverState.C2SKey));
	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  C2S key size:        {}", serverState.C2SKey.size());

	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  nonce ID:            {}", getItemID(nonce));
	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  nonce size:          {}", nonce.size());

	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  ciphertext ID:       {}", getItemID(encryptedContent));
	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  ciphertext size:     {}", encryptedContent.size());

	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  associated data ID:  {}", getItemID(ntpPacket.getSerializedPackage())); //TODO: getItemID() --> getHash()
	LOG_TRACE("Server: verifyReq: NTP packet (without AEAD EF):  associated size:     {}", ntpPacket.getSerializedPackage().size());


	if (aead.decrypt() == false)
	{
		LOG_ERROR("Server: verifyReq: decryption failed");
		status = false;
	}

	// save decrypted ext fields (if available)
	serverState.decryptedExtFields = aead.getPlaintext();


	return status;
}











std::vector<unsigned char> NtsServerNtp::createCookie(ServerState & serverState)
{
	// get master AEAD Algo, Key and ID
	AeadAlgorithm masterAeadAlgo = getServerAeadAlgo();
	unsigned short masterAeadKeyId = getServerAeadKeyId();
	std::vector<unsigned char> masterAeadKey = getServerAeadKey(masterAeadKeyId);

	// create plain cookie
	CookieContent plainCookie(serverState.negAeadAlgo, serverState.S2CKey, serverState.C2SKey);

	// create new nonce
	std::vector<unsigned char> nonce = getRandomNumber(NONCE_SIZE);
	if (nonce.size() == 0)
	{
		LOG_ERROR("createCookie: nonce is zero)");
	}

	// encrypt plain cookie
	AEAD aead;
	std::vector<unsigned char> encCookieContent; //aead out
	if (aead.isSupported(masterAeadAlgo) == true)
	{
		aead.setAeadAlgorithm(masterAeadAlgo);
		aead.setKey(masterAeadKey);
		aead.setPlaintext(plainCookie.getSerializedData());
		aead.setNonce(nonce);
		if (aead.encrypt() == false)
		{
			LOG_ERROR("createCookie: ENCRYPTION failed!");
		}
		else
		{
			encCookieContent = aead.getCiphertext();
		}
	}
	else
	{
		LOG_ERROR("createCookie: ENCRYPTION failed: aead algo not supported");
	}

	// create final cookie
	Cookie cookie(masterAeadKeyId, nonce, encCookieContent);
	if (cookie.isValid() == false)
	{
		LOG_ERROR("createCookie: cookie is invald");
	}

	return cookie.getSerializedData();
}




AeadAlgorithm NtsServerNtp::getServerAeadAlgo() const
{
	return m_settings.server.aeadAlgoForMasterKey;
}

std::vector<unsigned char> NtsServerNtp::getServerAeadKey(unsigned short keyId) const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerNtp::getServerAeadKey(): m_masterKey == nullptr");
		return std::vector<unsigned char>();
	}
	return m_masterKey->getKey(keyId);
}

unsigned short NtsServerNtp::getServerAeadKeyId() const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerNtp::getServerAeadKeyId(): m_masterKey == nullptr");
		return 0;
	}
	return m_masterKey->getCurrentKeyId();
}

