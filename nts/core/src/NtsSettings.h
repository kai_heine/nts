/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <string>
#include <vector>
#include "NtsConfig.h"
#include "cryptoFunctions.h"

typedef struct NtsSettings
{
	NtsSettings() :
		general(),
		console_logging(),
		file_logging(),
		server(),
		client()
	{
	}


	struct general
	{
		NtsMode ntsServiceMode = NtsMode::not_set;
	} general;

	struct console_logging
	{
		bool enableLogToConsole = false;
		std::string logLevel = "";
		bool advancedLogInfos = false;
		bool useOutputColor = false;
		bool asyncLogger = false;

	} console_logging;

	struct file_logging
	{
		bool enableLogToFile = false;
		std::string logLevel = "";
		std::string logFile = "";
		unsigned int maxLogFiles = 0;
		unsigned int maxLogFileSize = 0;
		bool dailyRotation = false;
		bool rotateOnStartup = false;
		bool advancedLogInfos = false;
	} file_logging;

	struct server
	{
		bool allowTls12 = false;
		unsigned long defaultKeServerTcpPort = 0;
		AeadAlgorithm aeadAlgoForMasterKey = AeadAlgorithm::NOT_DEFINED;
		MessageDigestAlgo mdAlgoForMasterKeyHkdf = MessageDigestAlgo::Not_Defined;
		unsigned long keyRotationHistory = 0;
		unsigned long keyRotationIntervalSeconds = 0;
		std::string serverCertChainFile = "";
		std::string serverPrivateKey = "";
		std::vector<NextProtocol> supportedNextProtocols;
		std::vector<AeadAlgorithm> supportedAeadAlgos;
		unsigned long maxAmountOfCookies = 0;
	} server;

	struct client
	{
		unsigned long tlsTimeoutSeconds = 0;
		bool allowTls12 = false;
		std::string rootCaBundleFile = "";
		std::vector<NextProtocol> supportedNextProtocols;
		std::vector<AeadAlgorithm> supportedAeadAlgos;
		unsigned long maxAmountOfCookies = 0;
	} client;



	
} NtsSettings;

NtsSettings readConfigFile(std::string const &configFile);

