/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>

class Record;
enum class AeadAlgorithm : unsigned short;

namespace RecType
{
	namespace AeadAlgoNeg
	{
		void format(Record &record);
		bool isValid(Record const &record);

		unsigned int countAlgorithms(Record const &record);
		void clearAlgorithms(Record &record);

		std::vector<unsigned short> getAlgorithmList(Record const &record);
		void setAlgorithmList(Record &record, std::vector<unsigned short> const &algorithmList);
		
		unsigned short getAlgorithmVal(Record const &record, unsigned int index);
		AeadAlgorithm getAlgorithm(Record const &record, unsigned int index);
		void addAlgorithm(Record &record, unsigned short algorithm);
		void addAlgorithm(Record &record, AeadAlgorithm algorithm);

		namespace
		{
			const unsigned short c_algorithmSize = 2; // 16-bit value
		}
	}
}
