﻿#include "Record.h"

#include "../Logger/Logger.h"
#include "../NtsConfig.h"


Record::Record()
	:
	m_criticalBit(false),
	m_recordType(0),
	m_recordBody()
{
}


Record::Record(bool critBit, unsigned short type, std::vector<unsigned char> const & body)
	:
	m_criticalBit(critBit)
{
	this->setType(type);
	this->setBody(body);
}


Record::Record(bool critBit, RecordType type, std::vector<unsigned char> const & body)
	: 
	m_criticalBit(critBit)
{
	this->setType(type);
	this->setBody(body);
}


/**
* @brief	Creates a new Record by parsing a binary record stream.
*/
Record::Record(std::vector<unsigned char> const & serializedData)
	:
	m_criticalBit(false),
	m_recordType(0)
{
	this->fromSerializedData(serializedData);
}


bool Record::getCriticalBit() const
{
	return m_criticalBit;
}


void Record::setCriticalBit(bool critBit)
{
	this->m_criticalBit = critBit;
}


unsigned short Record::getTypeVal() const
{
	return m_recordType;
}


RecordType Record::getType() const
{
	return static_cast<RecordType>(m_recordType);
}


void Record::setType(unsigned short type)
{
	if (type < 0x8000) // the MSB is reserved for the Critical Bit
	{
		this->m_recordType = type;
	}
	else
	{
		LOG_ERROR("the passed Record Type is invalid: {} (max: 32767)", type);
	}
}


void Record::setType(RecordType type)
{
	this->m_recordType = static_cast<unsigned short>(type);
}


const std::vector<unsigned char> & Record::getBody() const
{
	return m_recordBody;
}


/**
* @brief	Sets the body of the Record as a copy of the delivered parameter.
*/
void Record::setBody(std::vector<unsigned char> const & recordBody)
{
	if (recordBody.size() > 0xFFFF)
	{
		LOG_ERROR("invalid size of the Record Body: {} bytes (max: 65535)", recordBody.size());
		return;
	}

	this->m_recordBody = recordBody;
}


unsigned short Record::getBodyLength() const
{
	return static_cast<unsigned short>(m_recordBody.size());
}


/**
* @brief	Serializes the Record into a binary data stream.
*/
std::vector<unsigned char> Record::getSerializedData() const
{
	std::vector<unsigned char> recordStream;
	recordStream.resize(c_recordHeaderSize);

	// write Critical Bit (if necessary)
	if (m_criticalBit == true)
	{
		recordStream.at(0) |= 1 << 7;
	}

	// write Record Type
	recordStream.at(0) |= (m_recordType >> 8) & 0x7F;
	recordStream.at(1) |= m_recordType & 0xFF;

	// write Body Length
	unsigned short length = static_cast<unsigned short>(m_recordBody.size());
	recordStream.at(2) = (length >> 8) & 0xFF;
	recordStream.at(3) = length & 0xFF;

	// write Record Body
	recordStream.insert(recordStream.end(), m_recordBody.begin(), m_recordBody.end());

	return recordStream;
}


/**
* @brief	Parses the given binary stream into a Record.
*/
void Record::fromSerializedData(std::vector<unsigned char> const & serializedData)
{
	if (serializedData.size() < c_recordHeaderSize)
	{
		LOG_ERROR("invalid record size");
		return;
	}

	// parse binary stream
	bool criticalBit = (serializedData.at(0) >> 7) & 1;
	unsigned short recordType = ((serializedData.at(0) & 0x7f) << 8) | serializedData.at(1);
	unsigned short bodyLength = (serializedData.at(2) << 8) | serializedData.at(3);
	std::vector<unsigned char> recordBody(serializedData.begin() + c_recordHeaderSize, serializedData.end());

	// check values
	if (recordBody.size() != bodyLength)
	{
		LOG_ERROR("the NTS Record is corrupted");
		return;
	}

	// all is fine -> save data
	m_recordBody.clear();
	m_criticalBit = criticalBit;
	m_recordType = recordType;
	m_recordBody = std::move(recordBody);
}


/**
* @brief	Deletes the content of the Record.
*/
void Record::clear()
{
	m_criticalBit = false;
	m_recordType = 0;
	m_recordBody.clear();
}