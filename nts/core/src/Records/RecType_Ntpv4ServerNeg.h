/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

class Record;
#include <string>

namespace RecType
{
	namespace Ntpv4ServerNeg
	{
		void format(Record &record);
		bool isValid(Record const &record);

		const std::string getServer(Record const &record);
		void setServer(Record &record, std::string const &server);

		bool isIPv4Address(std::string const & host);
		bool isIPv6Address(std::string const & host);
		bool isHostname(std::string const & host);
	}
}