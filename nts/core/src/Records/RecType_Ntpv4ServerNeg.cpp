#include "RecType_Ntpv4ServerNeg.h"
#include <regex>
#include "../Logger/Logger.h"
#include "../NtsConfig.h"
#include "Record.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::Ntpv4ServerNeg::format(Record & record)
{
	record.clear();
	record.setCriticalBit(false);
	record.setType(RecordType::NTPv4_Server_Negotiation);
}

/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::Ntpv4ServerNeg::isValid(Record const & record)
{
	bool status = true;

	if (record.getType() != RecordType::NTPv4_Server_Negotiation)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() == 0)
	{
		LOG_WARN("invalid Body Length: 0 bytes");
		status = false;
	}
	else
	{
		std::vector<unsigned char> const & data = record.getBody();
		std::string serverStr(data.begin(), data.end());
		bool match = false;

		if (isIPv4Address(serverStr) == true)
		{
			match = true;
		}
		else if (isIPv6Address(serverStr) == true)
		{
			// NTS restriction: no zone IDs allowed (RFC6874)
			if (serverStr.find("%") != std::string::npos)
			{
				LOG_WARN("IPv6 zone identifiers are not allowed");
			}
			else
			{
				match = true;
			}
		}
		else if (isHostname(serverStr) == true)
		{
			match = true;
		}

		if (match == false)
		{
			LOG_ERROR("invalid server address");
			status = false;
		}
	}

	return status;
}



const std::string RecType::Ntpv4ServerNeg::getServer(Record const & record)
{
	std::vector<unsigned char> const & data = record.getBody();
	std::string serverStr(data.begin(), data.end());
	return serverStr;
}



void RecType::Ntpv4ServerNeg::setServer(Record &record, std::string const & server)
{
	if ((isIPv4Address(server) == true) || (isHostname(server) == true) || (isIPv6Address(server) == true))
	{
		std::vector<unsigned char> data(server.begin(), server.end());
		record.setBody(data);
	}
	else
	{
		LOG_WARN("invalid server address");
	}
}


bool RecType::Ntpv4ServerNeg::isIPv4Address(std::string const & host)
{
	try
	{
		const static std::regex IPv4
		(
			"^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}"
			"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",
			std::regex_constants::optimize
		);
		if (regex_match(host, IPv4))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (std::exception& e)
	{
		LOG_ERROR("regex exception: {}", e.what());
		return false;
	}
}


/*
	does not check the zone restriction
*/
bool RecType::Ntpv4ServerNeg::isIPv6Address(std::string const & host)
{
	/*
		It should match:
		--------------------
		- IPv6 addresses
		- Zero compressed IPv6 addresses (section 2.2 of RFC5952)
		- link-local IPv6 addresses with zone index (section 11 of RFC4007)
		- IPv4-Embedded IPv6 address (section 2 of RFC6052)
		- IPv4-mapped IPv6 addresses (section 2.1 of RFC2765)
		- IPv4-translated addresses (section 2.1 of RFC2765)
	*/

	try
	{
		const static std::regex IPv6
		(
			"([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|"           // 1:2:3:4:5:6:7:8
			"([0-9a-fA-F]{1,4}:){1,7}:|"                          // 1::                              1:2:3:4:5:6:7::
			"([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|"          // 1::8             1:2:3:4:5:6::8  1:2:3:4:5:6::8
			"([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|"   // 1::7:8           1:2:3:4:5::7:8  1:2:3:4:5::8
			"([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|"   // 1::6:7:8         1:2:3:4::6:7:8  1:2:3:4::8
			"([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|"   // 1::5:6:7:8       1:2:3::5:6:7:8  1:2:3::8
			"([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|"   // 1::4:5:6:7:8     1:2::4:5:6:7:8  1:2::8
			"[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|"        // 1::3:4:5:6:7:8   1::3:4:5:6:7:8  1::8  
			":((:[0-9a-fA-F]{1,4}){1,7}|:)|"                      // ::2:3:4:5:6:7:8  ::2:3:4:5:6:7:8 ::8
			"fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|"      // fe80::7:8%eth0   fe80::7:8%1     (link-local IPv6 addresses with zone index)

			"::(ffff(:0{1,4}){0,1}:){0,1}"                        // IPv4-mapped IPv6 addresses and IPv4-translated addresses
			"((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"  // ::255.255.255.255   ::ffff:255.255.255.255  ::ffff:0:255.255.255.255  
			"(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|"

			"([0-9a-fA-F]{1,4}:){1,4}:"                           // IPv4-Embedded IPv6 address
			"((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"  // 2001:db8:3:4::192.0.2.33  64:ff9b::192.0.2.33
			"(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])",
			std::regex_constants::optimize
		);
		if (regex_match(host, IPv6))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (std::exception& e)
	{
		LOG_ERROR("regex exception: {}", e.what());
		return false;
	}
}


bool RecType::Ntpv4ServerNeg::isHostname(std::string const & host)
{
	/*
		conform to RFC1123
		--------------------
		- The entire hostname (including the delimiting dots) has a maximum of 255 ASCII characters
		- Each label must be from 1 to 63 characters long (For example:  label1.label2.label3.com)
		- it supports:
			- fully qualified domain name (FQDN)
			- IPv4 address in dotted-decimal ("#.#.#.#") form
			- Internationalized Domain Names (A-Labels) --> https://tools.ietf.org/html/rfc5891#section-5.3


	*/
	if ((host.length() == 0) || (host.length() > 255))
	{
		return false;
	}
	try
	{
		const static std::regex regularHostname
		(
			"^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$",
			std::regex_constants::optimize
		);

		// Internationalized Domain Names (A-Labels)
		const static std::regex IdnaHostname
		(
			"^((?!-))(xn--)?[a-z0-9][a-z0-9-_]{0,61}[a-z0-9]{0,}\\.?((xn--)?([a-z0-9\\-.]{1,61}|[a-z0-9-]{1,30})\\.?[a-z]{2,})$",
			std::regex_constants::optimize
		);

		if (host.find("xn--") == 0)
		{
			if (regex_match(host, IdnaHostname))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (regex_match(host, regularHostname))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	catch (std::exception& e)
	{
		LOG_ERROR("regex exception: {}", e.what());
		return false;
	}
}