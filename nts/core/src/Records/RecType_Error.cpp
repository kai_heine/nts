#include "RecType_Error.h"
#include "../Logger/Logger.h"
#include "../NtsConfig.h"
#include "Record.h"

/**
 * @brief	Clears the content and sets the Record header according the rules of this type.
 */
void RecType::Error::format(Record &record)
{
	record.clear();
	record.setCriticalBit(true);
	record.setType(RecordType::Error);
}


/**
 * @brief	Checks if the record is valid and compliant with the NTS specification.
 */
bool RecType::Error::isValid(Record const &record)
{
	bool status = true;

	if (record.getCriticalBit() == false)
	{
		LOG_ERROR("Critical Bit is not set");
		status = false;
	}

	if (record.getType() != RecordType::Error)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() != 2)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}


unsigned short RecType::Error::getErrorVal(Record const &record)
{
	std::vector<unsigned char> const &rawError = record.getBody();
	return ((rawError.at(0) & 0xFF) << 8) | rawError.at(1);
}


RecordErr RecType::Error::getError(Record const &record)
{
	return static_cast<RecordErr>(getErrorVal(record));
}


void RecType::Error::setError(Record &record, unsigned short error)
{
	std::vector<unsigned char> raw;
	raw.resize(2);
	raw.at(0) |= (error >> 8) & 0xFF;
	raw.at(1) |= error & 0xFF;
	record.setBody(raw);
}


void RecType::Error::setError(Record &record, RecordErr error)
{
	setError(record, static_cast<unsigned short>(error));
}
