/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include <cstddef>


class Record;
enum class RecordType : unsigned short;

class RecordStack
{
public:
	RecordStack() = default;
	RecordStack(std::vector<unsigned char> const &serializedData);
	~RecordStack() = default;

	unsigned int getStackSize() const;
	const Record & getRecord(unsigned int index) const;
	unsigned int findRecord(RecordType type, unsigned int startIndex = 1) const;
	void addRecord(Record const &record);
	void clear();
	
	std::vector<unsigned char> getSerializedData() const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData);

private:
	const std::size_t c_recordHeaderSize = 4;
	std::vector<Record> m_RecordStack;
};