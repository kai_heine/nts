#include "RecStackVerification.h"
#include "../NtsConfig.h"
#include "../Logger/Logger.h"
#include "Record.h"
#include "RecType_EndOfMessage.h"
#include "RecType_NextProtNeg.h"
#include "RecType_Error.h"
#include "RecType_Warning.h"
#include "RecType_AeadAlgoNeg.h"
#include "RecType_NewCookie.h"
#include "RecType_Ntpv4ServerNeg.h"
#include "RecType_Ntpv4PortNeg.h"
#include "RecordStack.h"

enum class RecordStackType
{
	ClientRequest,
	ServerResponse
};

bool verifyEndOfMsg(RecordStack const &stack);
bool verifyNtsNextProtNeg(RecordStack const &stack);
bool verifyError(RecordStack const &stack, RecordStackType stackType);
bool verifyWarning(RecordStack const &stack, RecordStackType stackType);
bool verifyAeadAlgoNeg(RecordStack const &stack, bool ntpIsNextProtocol);
bool verifyNewCookie(RecordStack const &stack, RecordStackType stackType);
bool verifyNtpv4ServerNeg(RecordStack const &stack);
bool verifyNtpv4PortNeg(RecordStack const &stack);

bool verifyStackItems(RecordStack const &stack);
bool isNextProtocolPresent(RecordStack const &stack, NextProtocol nextProt);


/**
 * @brief	Checks if the stack is valid and compliant with the NTS specification.
 *
 * @param [in]	verifyItems		Performs an additional sanity check of all Records in the stack.
 */
bool RecStackVerification::clientRequestStackIsValid(RecordStack const &stack, bool verifyItems)
{
	bool status = true;

	if (verifyEndOfMsg(stack) == false)
	{
		status = false;
	}

	if (verifyNtsNextProtNeg(stack) == false)
	{
		status = false;
	}

	if (verifyError(stack, RecordStackType::ClientRequest) == false)
	{
		status = false;
	}

	if (verifyWarning(stack, RecordStackType::ClientRequest) == false)
	{
		status = false;
	}

	bool foundNextProt = isNextProtocolPresent(stack, NextProtocol::NTPv4);
	if (verifyAeadAlgoNeg(stack, foundNextProt) == false)
	{
		status = false;
	}

	if (verifyNewCookie(stack, RecordStackType::ClientRequest) == false)
	{
		status = false;
	}

	if (verifyNtpv4ServerNeg(stack) == false)
	{
		status = false;
	}

	if (verifyNtpv4PortNeg(stack) == false)
	{
		status = false;
	}

	if (verifyItems == true)
	{
		if (verifyStackItems(stack) == false)
		{
			status = false;
		}
	}

	return status;
}


/**
 * @brief	Checks if the stack is valid and compliant with the NTS specification.
 *
 * @param [in]	verifyItems		Performs an additional sanity check of all Records in the stack.
 */
bool RecStackVerification::serverResponseStackIsValid(RecordStack const &stack, bool verifyItems)
{
	bool status = true;

	if (verifyEndOfMsg(stack) == false)
	{
		status = false;
	}

	if (verifyNtsNextProtNeg(stack) == false)
	{
		status = false;
	}

	if (verifyError(stack, RecordStackType::ServerResponse) == false)
	{
		status = false;
	}

	if (verifyWarning(stack, RecordStackType::ServerResponse) == false)
	{
		status = false;
	}

	bool foundNextProt = isNextProtocolPresent(stack, NextProtocol::NTPv4);
	if (verifyAeadAlgoNeg(stack, foundNextProt) == false)
	{
		status = false;
	}

	if (verifyNewCookie(stack, RecordStackType::ServerResponse) == false)
	{
		status = false;
	}

	if (verifyItems == true)
	{
		if (verifyStackItems(stack) == false)
		{
			status = false;
		}
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyEndOfMsg(RecordStack const &stack)
{
	// request stack (client):  exact one Record;  must be the last Record in the stack
	// response stack (server): exact one Record;  must be the last Record in the stack

	bool status          = true;
	int stackSize        = stack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::End_of_Message)
		{
			counter++;
		}
	}
	if (counter == 0)
	{
		LOG_ERROR("missing Record: 'End of Message'");
		status = false;
	}
	else if (counter > 1)
	{
		LOG_ERROR("multiple Records received: 'End of Message'");
		status = false;
	}
	else
	{
		if (stack.getRecord(stackSize).getType() != RecordType::End_of_Message)
		{
			LOG_ERROR("invalid Record position: 'End of Message'");
			status = false;
		}
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyNtsNextProtNeg(RecordStack const &stack)
{
	// request stack (client):  exact one Record
	// response stack (server): exact one Record

	bool status          = true;
	int stackSize        = stack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::Next_Protocol_Negotiation)
		{
			counter++;
		}
	}
	if (counter == 0)
	{
		LOG_ERROR("missing Record: 'NTS Next Protocol Negotiation'");
		status = false;
	}
	else if (counter > 1)
	{
		LOG_ERROR("multiple Records received: 'NTS Next Protocol Negotiation'");
		status = false;
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyError(RecordStack const &stack, RecordStackType stackType)
{
	// request stack (client):  MUST NOT be included
	// response stack (server): not defined

	bool status = true;

	if (stackType == RecordStackType::ClientRequest)
	{
		int stackSize = stack.getStackSize();
		for (int i = 1; i <= stackSize; i++)
		{
			if (stack.getRecord(i).getType() == RecordType::Error)
			{
				LOG_ERROR("The client request contains an 'Error' Record. This is not allowed.");
				status = false;
				break;
			}
		}
	}
	else if (stackType == RecordStackType::ServerResponse)
	{
		status = true;
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyWarning(RecordStack const &stack, RecordStackType stackType)
{
	// request stack (client):  MUST NOT be included
	// response stack (server): not defined

	bool status = true;

	if (stackType == RecordStackType::ClientRequest)
	{
		int stackSize = stack.getStackSize();
		for (int i = 1; i <= stackSize; i++)
		{
			if (stack.getRecord(i).getType() == RecordType::Warning)
			{
				LOG_ERROR("The client request contains a 'Warning' Record. This is not allowed.");
				status = false;
				break;
			}
		}
	}
	else if (stackType == RecordStackType::ServerResponse)
	{
		status = true;
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyAeadAlgoNeg(RecordStack const &stack, bool ntpIsNextProtocol)
{
	// request stack (client):  exact one Record if 'NTPv4' the negotiated Next Protocol
	// response stack (server): exact one Record if 'NTPv4' the negotiated Next Protocol

	bool status          = true;
	int stackSize        = stack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::AEAD_Algorithm_Negotiation)
		{
			counter++;
		}
	}

	if ((counter == 0) && (ntpIsNextProtocol == true))
	{
		LOG_ERROR("missing Record: 'AEAD_Algorithm_Negotiation'");
		status = false;
	}
	else if ((counter > 1) && (ntpIsNextProtocol == true))
	{
		LOG_ERROR("multiple Records received: 'AEAD_Algorithm_Negotiation'");
		status = false;
	}

	return status;
}


/**
 * @return	Returns 'true' if the number and position of the specific Record match
 *           with the NTS specification
 */
bool verifyNewCookie(RecordStack const &stack, RecordStackType stackType)
{
	// request stack (client):  MUST NOT be included
	// response stack (server): not defined

	bool status = true;

	if (stackType == RecordStackType::ClientRequest)
	{
		int stackSize = stack.getStackSize();
		for (int i = 1; i <= stackSize; i++)
		{
			if (stack.getRecord(i).getType() == RecordType::New_Cookie_for_NTPv4)
			{
				LOG_ERROR(
				    "The client request contains a 'New_Cookie_for_NTPv4' Record. This is not allowed.");
				status = false;
				break;
			}
		}
	}
	else if (stackType == RecordStackType::ServerResponse)
	{
		status = true;
	}


	return status;
}


/**
 * @return	Returns 'true' if the number of the specific Record match
 *           with the NTS specification
 */
bool verifyNtpv4ServerNeg(RecordStack const &stack)
{
	// request stack (client):  max. one Record;  optional
	// response stack (server): max. one Record;  optional

	bool status          = true;
	int stackSize        = stack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::NTPv4_Server_Negotiation)
		{
			counter++;
		}
	}
	if (counter > 1)
	{
		LOG_ERROR("multiple Records received: 'NTPv4 Server Negotiation'");
		status = false;
	}

	return status;
}


/**
 * @return	Returns 'true' if the number of the specific Record match
 *           with the NTS specification
 */
bool verifyNtpv4PortNeg(RecordStack const &stack)
{
	// request stack (client):  max. one Record;  optional
	// response stack (server): max. one Record;  optional

	bool status          = true;
	int stackSize        = stack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::NTPv4_Port_Negotiation)
		{
			counter++;
		}
	}
	if (counter > 1)
	{
		LOG_ERROR("multiple Records received: 'NTPv4 Port Negotiation'");
		status = false;
	}

	return status;
}



/**
 * @brief	Performs a sanity check of all Records in the stack (record.isValid()).
 *
 * @return	Returns 'true' if all Records pass the verification.
 */
bool verifyStackItems(RecordStack const &stack)
{
	bool status = true;

	int stackSize = stack.getStackSize();
	for (int i = 1; i <= stackSize; i++)
	{
		const Record &rec = stack.getRecord(i);
		RecordType type   = rec.getType();

		switch (type)
		{
		case RecordType::End_of_Message:
			if (RecType::EndOfMessage::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::Next_Protocol_Negotiation:
			if (RecType::NextProtNeg::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::Error:
			if (RecType::Error::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::Warning:
			if (RecType::Warning::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::AEAD_Algorithm_Negotiation:
			if (RecType::AeadAlgoNeg::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::New_Cookie_for_NTPv4:
			if (RecType::NewCookie::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::NTPv4_Server_Negotiation:
			if (RecType::Ntpv4ServerNeg::isValid(rec) == false)
			{
				status = false;
			}
			break;

		case RecordType::NTPv4_Port_Negotiation:
			if (RecType::Ntpv4PortNeg::isValid(rec) == false)
			{
				status = false;
			}
			break;

		default:
			LOG_WARN("unknown Record found. Record Type: {}", rec.getTypeVal());

			if (rec.getCriticalBit() == true)
			{
				status = false;
			}
			else
			{
				status = true; // ignore unknown Records
			}
		}
	}
	return status;
}


/**
 * @return	Returns 'true' if the stack contains the given Next Protocol
 */
bool isNextProtocolPresent(RecordStack const &stack, NextProtocol nextProt)
{
	bool found    = false;
	int stackSize = stack.getStackSize();

	for (int i = 1; i <= stackSize; i++)
	{
		if (stack.getRecord(i).getType() == RecordType::Next_Protocol_Negotiation)
		{
			const Record &rec = stack.getRecord(i);
			if (RecType::NextProtNeg::isValid(rec) == true)
			{
				int count = RecType::NextProtNeg::countProtocols(rec);
				for (int j = 1; i <= count; i++)
				{
					if (RecType::NextProtNeg::getProtocol(rec, j) == nextProt)
					{
						found = true;
					}
				}
			}
		}
	}

	return found;
}