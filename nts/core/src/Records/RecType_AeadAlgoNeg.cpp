#include "RecType_AeadAlgoNeg.h"
#include "../Logger/Logger.h"
#include "../NtsConfig.h"
#include "Record.h"
#include <cstddef>
/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::AeadAlgoNeg::format(Record & record)
{
	record.clear();
	record.setCriticalBit(true);
	record.setType(RecordType::AEAD_Algorithm_Negotiation);
}


/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::AeadAlgoNeg::isValid(Record const & record)
{
	bool status = true;

	if (record.getType() != RecordType::AEAD_Algorithm_Negotiation)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if ((record.getBodyLength() % c_algorithmSize) != 0)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}


unsigned int RecType::AeadAlgoNeg::countAlgorithms(Record const & record)
{
	return record.getBodyLength() / c_algorithmSize;
}


/**
* @brief	Sets the algorithm list of the Record (as a copy of the delivered list).
*/
void RecType::AeadAlgoNeg::setAlgorithmList(Record & record, std::vector<unsigned short> const &algorithmList)
{
	std::vector<unsigned char> listVector;
	for (auto const& item : algorithmList)
	{
		std::vector<unsigned char> tmp(2);
		tmp.at(0) |= (item >> 8) & 0xFF;
		tmp.at(1) |= item & 0xFF;

		listVector.insert(listVector.end(), tmp.begin(), tmp.end());
	}
	record.setBody(listVector);
}


/**
* @brief	Deletes all algorithms of the Record
*/
void RecType::AeadAlgoNeg::clearAlgorithms(Record & record)
{
	record.setBody(std::vector<unsigned char>());
}


std::vector<unsigned short> RecType::AeadAlgoNeg::getAlgorithmList(Record const & record)
{
	std::vector<unsigned char> const &rawList = record.getBody();
	std::vector<unsigned short> finalList;

	for (std::size_t i = 0; i < rawList.size(); i += c_algorithmSize)
	{
		if ((rawList.size() - i) < c_algorithmSize)
		{
			LOG_ERROR("invalid Body Length: {} bytes", rawList.size());
			return std::vector<unsigned short>();
		}

		unsigned short tmp = ((rawList.at(i) & 0xFF) << 8) | rawList.at(i + 1);
		finalList.push_back(tmp);
	}
	return finalList;
}


void RecType::AeadAlgoNeg::addAlgorithm(Record & record, unsigned short algorithm)
{
	std::vector<unsigned char> item(2);
	item.at(0) |= (algorithm >> 8) & 0xFF;
	item.at(1) |= algorithm & 0xFF;

	std::vector<unsigned char> listVector(record.getBody());
	listVector.insert(listVector.end(), item.begin(), item.end());
	record.setBody(listVector);
}


void RecType::AeadAlgoNeg::addAlgorithm(Record & record, AeadAlgorithm algorithm)
{
	addAlgorithm(record, static_cast<unsigned short>(algorithm));
}


/**
* @param [in]	index	Index (position) of the algorithm (starts at 1).
*/
unsigned short RecType::AeadAlgoNeg::getAlgorithmVal(Record const & record, unsigned int index)
{
	std::vector<unsigned short> protList = getAlgorithmList(record);

	if ((index < 1) || (index > protList.size()))
	{
		LOG_ERROR("index is out of range");
		return 0;
	}
	return protList.at(index - 1);
}


/**
* @param [in]	index	Index (position) of the algorithm (starts at 1).
*/
AeadAlgorithm RecType::AeadAlgoNeg::getAlgorithm(Record const & record, unsigned int index)
{
	return static_cast<AeadAlgorithm>(getAlgorithmVal(record, index));
}
