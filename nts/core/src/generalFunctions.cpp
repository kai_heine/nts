#include "generalFunctions.h"
#include <iomanip>
#include <sstream>
#include <cstddef>

// convert binary data to text (hex view)
std::string binToHexStr(unsigned char const *buf, int num, std::string pairDelimiter, int newLineNum, bool uppercase)
{
	std::stringstream stream;

	// clear strings
	stream.str("");

	// check parameters
	if (buf == nullptr || num <= 0)
	{
		return "";
	}

	if (newLineNum < 0)
	{
		newLineNum = 0;
	}

	// convert binary data to string
	for (int i = 0; i <= num - 1; i++)
	{
		if (uppercase == true)
		{
			stream << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int) buf [i] << std::flush;
		}
		else
		{
			stream << std::setw(2) << std::setfill('0') << std::hex << (int) buf [i] << std::flush;
		}


		// add delimiter
		if (i < num - 1)
		{
			stream << pairDelimiter.c_str() << std::flush;
		}

		// add word wrap
		if (newLineNum != 0)
		{
			if ((i + 1) % newLineNum == 0)
			{
				stream << std::endl << std::flush;
			}
		}
	}

	// return hex string
	return stream.str();
}



// convert binary data to text (hex view)
std::string binToHexStr(std::vector<unsigned char> const &data, std::string pairDelimiter, int newLineNum, bool uppercase)
{
	return binToHexStr(data.data(), static_cast<int>(data.size()), pairDelimiter, newLineNum, uppercase);
}





/*
* [in] string
* [out] tokens
* [in] delimiters (z.B. ",; " --> 3 delimiters)
*/
void parseString(const std::string &string, std::vector<std::string> &tokens, const std::string &delimiter)
{

	tokens.clear();
	std::size_t lastPos = string.find_first_not_of(delimiter, 0); 		// skip delimiters at beginning
	std::size_t currentPos = string.find_first_of(delimiter, lastPos);	// find first non-delimiter
	while (std::string::npos != currentPos || std::string::npos != lastPos)
	{
		tokens.push_back(string.substr(lastPos, currentPos - lastPos));	// add a found token to the vector
		lastPos = string.find_first_not_of(delimiter, currentPos);		// skip delimiter
		currentPos = string.find_first_of(delimiter, lastPos);			// find next non-delimiter
	}
	return;
}

