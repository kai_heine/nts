#include "NtpPacketInfoImpl.h"
#include "Logger/Logger.h"
#include "NtsConfig.h"


NtpPacketInfoImpl::NtpPacketInfoImpl()
	:
	m_errorCode(0)
{
}

void NtpPacketInfoImpl::setServer(std::string const& ntsKeServerIp, std::string const& ntpTimeServerIp, unsigned short ntpTimeServerPort)
{
	m_ntsKeServerIp = ntsKeServerIp;
	m_ntpTimeServerIp = ntpTimeServerIp;
	m_ntpTimeServerPort = ntpTimeServerPort == 0 ? NTP_DEFAULT_UDP_PORT : ntpTimeServerPort;
}

std::string const & NtpPacketInfoImpl::getNtsKeServerIp() const
{
	return m_ntsKeServerIp;
}

std::string const & NtpPacketInfoImpl::getNtpTimeServerIp() const
{
	return m_ntpTimeServerIp;
}

unsigned short NtpPacketInfoImpl::getNtpTimeServerPort() const
{
	return m_ntpTimeServerPort;
}

void NtpPacketInfoImpl::setErrorCode(int ec)
{
	m_errorCode = ec;
}

int NtpPacketInfoImpl::getErrorCode() const
{
	return m_errorCode;
}

void NtpPacketInfoImpl::setCurrentNtsContentLength(std::size_t length)
{
	std::size_t maxExtFieldSpace = 65487; // 2^16 - 1 - 48 (ntp header)
	if (length > maxExtFieldSpace)
	{
		LOG_ERROR("invalid NTS content size (currentNtsContentLength): {} bytes (max: 65487 bytes)", length);
		m_errorCode = -1;
		m_currentNtsContentLength = 0;
	}
	else
	{
		m_currentNtsContentLength = length;
	}
	
}

std::size_t NtpPacketInfoImpl::getCurrentNtsContentLength() const
{
	return m_currentNtsContentLength;
}


void NtpPacketInfoImpl::setNtsContentLengthNeeded(std::size_t length)
{
	std::size_t maxExtFieldSpace = 65487; // 2^16 - 1 - 48 (ntp header)
	if (length > maxExtFieldSpace)
	{
		LOG_ERROR("invalid NTS content size (ntsContentLengthNeeded): {} bytes (max: 65487 bytes)", length);
		m_errorCode = -1;
		m_ntsContentLengthNeeded = 0;
	}
	else
	{
		m_ntsContentLengthNeeded = length;
	}

}


std::size_t NtpPacketInfoImpl::getNtsContentLengthNeeded() const
{
	return m_ntsContentLengthNeeded;
}



void NtpPacketInfoImpl::setNtpPacket(unsigned char * ntpPacket, std::size_t packetLen, std::size_t bufferSize, bool rescan)
{
	m_ntpPacket.setNtpPacket(ntpPacket, packetLen, bufferSize);
	if (rescan == true)
	{
		m_ntpPacket.rescan();
	}
}


NtpPacket & NtpPacketInfoImpl::getNtpPacket()
{
	return m_ntpPacket;
}


ServerState & NtpPacketInfoImpl::getServerState()
{
	return m_serverState;
}

