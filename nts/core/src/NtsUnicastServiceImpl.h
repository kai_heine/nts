/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include "NtsSettings.h"
#include "./NtsClient/NtsClient.h"
#include "./NtsServer/NtsServer.h"


struct NtpPacketInfoImpl;
enum class NtsMode;


enum class NtsState
{
	not_init,
	init
};


struct NtsUnicastServiceImpl
{
public:
	NtsUnicastServiceImpl();
	~NtsUnicastServiceImpl() = default;
	NtsUnicastServiceImpl (NtsUnicastServiceImpl const &) = delete;
	NtsUnicastServiceImpl &operator=(NtsUnicastServiceImpl const &) = delete;

	void initialize(std::string const &globalConfigFile);

	void processAndVerify(NtpPacketInfoImpl& ntpPacketInfo);
	void createMessage_init(NtpPacketInfoImpl& ntpPacketInfo);
	void createMessage_update(NtpPacketInfoImpl& ntpPacketInfo);
	void createMessage_final(NtpPacketInfoImpl& ntpPacketInfo);

	bool isInit();
	bool isClientMode();
	bool isServerMode();
	void showInfos();

private:
	std::string m_configFile;
	NtsSettings m_settings;
	NtsClient m_ntsClient;
	NtsServer m_ntsServer;
	NtsMode m_ntsMode;
	NtsState m_ntsState;
};
