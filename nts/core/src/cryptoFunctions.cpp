#include "cryptoFunctions.h"

#include <openssl/evp.h>
#include <openssl/rand.h>
#include "./Logger/Logger.h"
#include <boost/algorithm/string.hpp>
#include "generalFunctions.h"
#include "NtsConfig.h"


/**
* @brief Delete function for the OpenSSL structure 'EVP_MD_CTX' (used by std::unique_ptr)
*/
struct EVP_MD_CTX_deleter
{
	void operator()(EVP_MD_CTX *ptr)
	{
		if (ptr != nullptr)
		{
			EVP_MD_CTX_free(ptr);
		}
	}
};
typedef std::unique_ptr<EVP_MD_CTX, EVP_MD_CTX_deleter> EVP_MD_CTX_Guard;



std::vector<unsigned char> getRandomNumber(unsigned int num) // CSPRNG
{
	std::vector<unsigned char> random;
	random.resize(num);

	// generate a cryptographically secure random number
	if (RAND_bytes(random.data(), static_cast<int>(random.size())) != 1)
	{
		LOG_ERROR("The random number generator (CSPRNG) was not seeded.");
		throw std::runtime_error("The random number generator (CSPRNG) was not seeded.");
	}
	return random;
}


void getRandomNumber(unsigned char *data, unsigned int num) // CSPRNG
{
	// generate a cryptographically secure random number
	if (RAND_bytes(data, num) != 1)
	{
		LOG_ERROR("The random number generator (CSPRNG) was not seeded.");
		throw std::runtime_error("The random number generator (CSPRNG) was not seeded.");
	}
}


/**
* @brief Returns the hash value of the given data.
*
* @param [in]  data			The source data.
* @param [in]  hashAlgoNid		The hash algorithm that is to be used (must be a NID). Some possible algorithms:
*                              NID_md4, NID_md5, NID_sha, NID_sha1, NID_sha224, NID_sha256, NID_sha384,
*                              NID_sha512, NID_ripemd160
*
* @returns Returns the fingerprint value.
*
* @throws NTS_EXCEPTION 	in case of error.
*/
std::vector<unsigned char> getFingerprint(std::vector<unsigned char> const &data, MessageDigestAlgo msgDigest)
{
	const EVP_MD *digest = nullptr;
	std::vector<unsigned char> fingerprint;
	EVP_MD_CTX_Guard digestContext(nullptr);
	unsigned int mdSize = 0;

	if (data.size() == 0)
	{
		LOG_ERROR("parameter 'data' is invalid");
		return std::vector<unsigned char>();
	}

	digest = EVP_get_digestbynid(static_cast<int>(msgDigest));
	if (digest == nullptr)
	{
		LOG_ERROR("Failed to get the message digest algorithm by the NID (numerical identifier): variable 'digest' is a nullptr");
		return std::vector<unsigned char>();
	}

	digestContext.reset(EVP_MD_CTX_new());
	if (digestContext == nullptr)
	{
		LOG_ERROR("Failed to create a new message-digest-context (EVP_MD_CTX) object.");
		return std::vector<unsigned char>();
	}

	if (EVP_DigestInit_ex(digestContext.get(), digest, nullptr) != 1)
	{
		LOG_ERROR("Failed to set the digest algorithm to the message-digest-context (EVP_MD_CTX) object.");
		return std::vector<unsigned char>();
	}

	if (EVP_DigestUpdate(digestContext.get(), data.data(), data.size()) != 1)
	{
		LOG_ERROR("Failed to set the message to the message-digest-context (EVP_MD_CTX) object.");
		return std::vector<unsigned char>();
	}

	fingerprint.resize(EVP_MAX_MD_SIZE);

	if (EVP_DigestFinal_ex(digestContext.get(), fingerprint.data(), &mdSize) != 1)
	{
		LOG_ERROR("Failed to calculate the hash value.");
		return std::vector<unsigned char>();
	}

	fingerprint.resize(mdSize);
	return fingerprint;
}


// liefert pseudo-ID von Binärdaten (fingerprint)
std::string getItemID(std::vector<unsigned char> const & data, unsigned int size)
{
	std::vector<unsigned char> fingerprint = getFingerprint(data, MessageDigestAlgo::SHA1);
	fingerprint.resize(size);// TODO: set max size?
	return binToHexStr(fingerprint, "", size+1, true);
}


void secureErase(std::vector<unsigned char> & data)
{
	if (data.size() > 0)
	{
		getRandomNumber(data.data(), static_cast<int>(data.size()));
		data.clear();
	}
}


unsigned int determineKeySize(AeadAlgorithm aeadAlgorithm)
{
	switch (aeadAlgorithm)
	{
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256:
		return 32;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384:
		return 48;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512:
		return 64;

	default:
		LOG_WARN("NtsServerKe: determineTlsKeySize: aeadAlgorithm not supported");
		return 0;
	}
}


AeadAlgorithm getAeadAlgorithmFromStr(std::string const & algorithmStr)
{
	if (boost::iequals(algorithmStr, "AEAD_AES_SIV_CMAC_256"))
	{
		return AeadAlgorithm::AEAD_AES_SIV_CMAC_256;
	}
	else if (boost::iequals(algorithmStr, "AEAD_AES_SIV_CMAC_384"))
	{
		return AeadAlgorithm::AEAD_AES_SIV_CMAC_384;
	}
	else if (boost::iequals(algorithmStr, "AEAD_AES_SIV_CMAC_512"))
	{
		return AeadAlgorithm::AEAD_AES_SIV_CMAC_512;
	}
	else
	{
		LOG_WARN("getAeadAlgorithm: aeadAlgorithm ({}) not supported", algorithmStr);
		return AeadAlgorithm::NOT_DEFINED;
	}
}



MessageDigestAlgo getMdAlgorithmFromStr(std::string const & algorithmStr)
{
	if (boost::iequals(algorithmStr, "SHA256"))
	{
		return MessageDigestAlgo::SHA256;
	}
	else if (boost::iequals(algorithmStr, "SHA384"))
	{
		return MessageDigestAlgo::SHA384;
	}
	else if (boost::iequals(algorithmStr, "SHA512"))
	{
		return MessageDigestAlgo::SHA512;
	}
	else if (boost::iequals(algorithmStr, "RIPEMD_160"))
	{
		return MessageDigestAlgo::RIPEMD_160;
	}
	else
	{
		LOG_WARN("getMdAlgorithm: MessageDigestAlgo ({}) not supported", algorithmStr);
		return MessageDigestAlgo::Not_Defined;
	}
}


NextProtocol getNextProtocolFromStr(std::string const & protocolStr)
{
	if (boost::iequals(protocolStr, "NTPv4")) // case insensitive compare
	{
		return NextProtocol::NTPv4;
	}
	else
	{
		LOG_WARN("getNextProtocol: protocol ({}) not supported", protocolStr);
		return NextProtocol::Undefined;
	}
}


std::string getAeadAlgorithmStr(AeadAlgorithm const & AeadAlgorithm)
{
	switch (AeadAlgorithm)
	{
	//https://www.iana.org/assignments/aead-parameters/aead-parameters.xhtml
	// RFC 5116
	case AeadAlgorithm::AEAD_AES_128_GCM: return "AEAD_AES_128_GCM";
	case AeadAlgorithm::AEAD_AES_256_GCM: return "AEAD_AES_256_GCM";
	case AeadAlgorithm::AEAD_AES_128_CCM: return "AEAD_AES_128_CCM";
	case AeadAlgorithm::AEAD_AES_256_CCM: return "AEAD_AES_256_CCM";

	// RFC 5282
	case AeadAlgorithm::AEAD_AES_128_GCM_8: return "AEAD_AES_128_GCM_8";
	case AeadAlgorithm::AEAD_AES_256_GCM_8: return "AEAD_AES_256_GCM_8";
	case AeadAlgorithm::AEAD_AES_128_GCM_12: return "AEAD_AES_128_GCM_12";
	case AeadAlgorithm::AEAD_AES_256_GCM_12: return "AEAD_AES_256_GCM_12";
	case AeadAlgorithm::AEAD_AES_128_CCM_SHORT: return "AEAD_AES_128_CCM_SHORT";
	case AeadAlgorithm::AEAD_AES_256_CCM_SHORT: return "AEAD_AES_256_CCM_SHORT";
	case AeadAlgorithm::AEAD_AES_128_CCM_SHORT_8: return "AEAD_AES_128_CCM_SHORT_8";
	case AeadAlgorithm::AEAD_AES_256_CCM_SHORT_8: return "AEAD_AES_256_CCM_SHORT_8";
	case AeadAlgorithm::AEAD_AES_128_CCM_SHORT_12: return "AEAD_AES_128_CCM_SHORT_12";
	case AeadAlgorithm::AEAD_AES_256_CCM_SHORT_12: return "AEAD_AES_256_CCM_SHORT_12";

	// RFC 5297
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256: return "AEAD_AES_SIV_CMAC_256";
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384: return "AEAD_AES_SIV_CMAC_384";
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512: return "AEAD_AES_SIV_CMAC_512";
	
	// RFC 6655
	case AeadAlgorithm::AEAD_AES_128_CCM_8: return "AEAD_AES_128_CCM_8";
	case AeadAlgorithm::AEAD_AES_256_CCM_8: return "AEAD_AES_256_CCM_8";
	
	// RFC 7253, Section 3.1
	case AeadAlgorithm::AEAD_AES_128_OCB_TAGLEN128: return "AEAD_AES_128_OCB_TAGLEN128";
	case AeadAlgorithm::AEAD_AES_128_OCB_TAGLEN96: return "AEAD_AES_128_OCB_TAGLEN96";
	case AeadAlgorithm::AEAD_AES_128_OCB_TAGLEN64: return "AEAD_AES_128_OCB_TAGLEN64";
	case AeadAlgorithm::AEAD_AES_192_OCB_TAGLEN128: return "AEAD_AES_192_OCB_TAGLEN128";
	case AeadAlgorithm::AEAD_AES_192_OCB_TAGLEN96: return "AEAD_AES_192_OCB_TAGLEN96";
	case AeadAlgorithm::AEAD_AES_192_OCB_TAGLEN64: return "AEAD_AES_192_OCB_TAGLEN64";
	case AeadAlgorithm::AEAD_AES_256_OCB_TAGLEN128: return "AEAD_AES_256_OCB_TAGLEN128";
	case AeadAlgorithm::AEAD_AES_256_OCB_TAGLEN96: return "AEAD_AES_256_OCB_TAGLEN96";
	case AeadAlgorithm::AEAD_AES_256_OCB_TAGLEN64: return "AEAD_AES_256_OCB_TAGLEN64";
	
	// RFC 7539
	case AeadAlgorithm::AEAD_CHACHA20_POLY1305: return "AEAD_CHACHA20_POLY1305";

	default: return "AEAD_NOT_DEFINED";
	}
}


std::string getMdAlgorithmStr(MessageDigestAlgo const & MessageDigestAlgo)
{
	switch (MessageDigestAlgo)
	{
	case MessageDigestAlgo::MD4: return "MD4";
	case MessageDigestAlgo::MD5: return "MD5";
	case MessageDigestAlgo::SHA1: return "SHA1";
	case MessageDigestAlgo::SHA224: return "SHA224";
	case MessageDigestAlgo::SHA256: return "SHA256";
	case MessageDigestAlgo::SHA384: return "SHA384";
	case MessageDigestAlgo::SHA512: return "SHA512";
	case MessageDigestAlgo::RIPEMD_160: return "RIPEMD_160";
	default: return "MD_NOT_DEFINED";
	}
}


std::string getNextProtocolStr(NextProtocol const & NextProtocol)
{
	switch (NextProtocol)
	{
	case NextProtocol::NTPv4: return "NTPv4";
	default: return "NextProtocol_NOT_DEFINED";
	}
}


bool isSupported(AeadAlgorithm algorithm)
{
	switch (algorithm)
	{
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256: return true;
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384: return true;
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512: return true;
	default: return false;
	}
}


bool isSupported(MessageDigestAlgo algorithm)
{
	switch (algorithm)
	{
	case MessageDigestAlgo::SHA256: return true;
	case MessageDigestAlgo::SHA384: return true;
	case MessageDigestAlgo::SHA512: return true;
	case MessageDigestAlgo::RIPEMD_160: return true;
	default: return false;
	}
}


bool isSupported(NextProtocol protocol)
{
	switch (protocol)
	{
	case NextProtocol::NTPv4: return true;
	default: return false;
	}
}
