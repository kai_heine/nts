/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once


#include <memory>
#include <vector>
#include <cstddef>

enum class AeadAlgorithm : unsigned short;

/*
   This is an AEAD wrapper for the 'libaes_siv' (https://github.com/dfoxfranke/libaes_siv) 
   library, which supports the following algorithms: AEAD_AES_SIV_CMAC_256, AEAD_AES_SIV_CMAC_384, 
   AEAD_AES_SIV_CMAC_512. 


   Short information:
   ----------------------------
   The AEAD algorithm depends on the key length used. For AEAD_AES_SIV_CMAC_256 a 256 bit key 
   is needed, for AEAD_AES_SIV_CMAC_384 a 384 bit key, and so on.

   Ciphertext = AEAD(Key, Nonce, Plaintext, Associated Data)
   Plaintext = AEAD(Key, Nonce, Ciphertext, Associated Data)

   The parameters 'Nonce','Plaintext' and 'Associated Data' are optional and can be zero. The 
   'Ciphertext' consists of an Authentication Tag and the encrypted plaintext. The length of the 
   Authentication Tag is always 16 bytes and the length of the 'encrypted plaintext' is always 
   identical to the length of the Plaintext parameter. If no Plaintext parameter is specified 
   then the Ciphertext contains the Authentication Tag only. The composition is as follows:

   Ciphertext = {Authentication Tag || encrypted plaintext}

   more detail: 
   https://tools.ietf.org/html/rfc5297
*/
class AEAD
{
public:
	AEAD() = default;
	~AEAD() = default;

	// AEAD algorithm
	void setAeadAlgorithm(AeadAlgorithm algorithm);
	void setAeadAlgorithm(unsigned short algorithm);
	AeadAlgorithm getAeadAlgorithm() const;
	unsigned short getAeadAlgorithmID() const;

	// Key
	void setKey(std::vector<unsigned char> const& key);
	const std::vector<unsigned char>& getKey() const;
	unsigned int getKeySize();

	// Nonce
	void setNonce(std::vector<unsigned char> const& nonce);
	const std::vector<unsigned char>& getNonce() const;
	unsigned int getNonceSize();

	// Plaintext
	void setPlaintext(std::vector<unsigned char> const& plaintext);
	const std::vector<unsigned char>& getPlaintext() const;
	unsigned int getPlaintextSize();

	// Associated Data
	void setAssociatedData(std::vector<unsigned char> const& associatedData);
	const std::vector<unsigned char>& getAssociatedData() const;
	unsigned int getAssociatedDataSize();

	// Ciphertext
	void setCiphertext(std::vector<unsigned char> const& ciphertext);
	const std::vector<unsigned char>& getCiphertext() const;
	unsigned int getCiphertextSize();

	// cryptographic operations
	bool encrypt();
	bool decrypt();

	static bool directEncrypt(AeadAlgorithm aeadAlgorithm,
	                          std::vector<unsigned char> const& key,
	                          std::vector<unsigned char> const& plaintext,
	                          std::vector<unsigned char>& ciphertext,
	                          std::vector<unsigned char> const& nonce,
	                          std::vector<unsigned char> const& associatedData);

	static bool directDecrypt(AeadAlgorithm aeadAlgorithm,
	                          std::vector<unsigned char> const& key,
	                          std::vector<unsigned char>& plaintext,
	                          std::vector<unsigned char> const& ciphertext,
	                          std::vector<unsigned char> const& nonce,
	                          std::vector<unsigned char> const& associatedData);

	// Misc
	void clear();
	static bool isSupported(AeadAlgorithm aeadAlgorithm);
	static bool isSupported(unsigned short aeadAlgorithm);

	static std::size_t getAuthTagLen(AeadAlgorithm aeadAlgorithm); // gibt die L�nge des Authentication Tag zur�ck

	static unsigned int getMaxNonceLength(AeadAlgorithm aeadAlgorithm);
	static unsigned int getMaxNonceLength(unsigned short aeadAlgorithm);

	static bool isAlgorithmAndKeyValid(AeadAlgorithm algorithm, std::size_t keySize);

private:
	static const unsigned int c_authTagSize = 16;            // this authentication tag is always 16 bytes
	static const unsigned int c_unlimitedNoneLength = 65535; // TODO: alternative?

	AeadAlgorithm m_AeadAlgorithm;
	std::vector<unsigned char> m_key;
	std::vector<unsigned char> m_nonce;
	std::vector<unsigned char> m_plaintext;
	std::vector<unsigned char> m_associatedData;
	std::vector<unsigned char> m_ciphertext;
};
