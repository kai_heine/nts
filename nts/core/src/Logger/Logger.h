/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

struct NtsSettings;
void configureLogger(const NtsSettings &settings);

#ifdef NTS_ENABLE_LOGGING
#include <spdlog/spdlog.h>

spdlog::logger *Logger();

#define CALL_LOGGER(level, ...)                                                                    \
    if (Logger()->should_log(level)) {                                                             \
        SPDLOG_LOGGER_CALL(Logger(), level, __VA_ARGS__);                                          \
    }


#ifdef NTS_ENABLE_TRACE_LOGGING
#define CONDITIONAL_TRACE(X)                                                                       \
    if (Logger()->should_log(spdlog::level::trace)) {                                              \
        X                                                                                          \
    }
#define LOG_TRACE(...) CALL_LOGGER(spdlog::level::trace, __VA_ARGS__)
#else
#define CONDITIONAL_TRACE(X) ((void)0)
#define LOG_TRACE(...) ((void)0)
#endif // NTS_ENABLE_TRACE_LOGGING


#define LOG_DEBUG(...) CALL_LOGGER(spdlog::level::debug, __VA_ARGS__)
#define LOG_INFO(...) CALL_LOGGER(spdlog::level::info, __VA_ARGS__)
#define LOG_WARN(...) CALL_LOGGER(spdlog::level::warn, __VA_ARGS__)
#define LOG_ERROR(...) CALL_LOGGER(spdlog::level::err, __VA_ARGS__)
#define LOG_FATAL(...) CALL_LOGGER(spdlog::level::critical, __VA_ARGS__)

#else

#define LOG_TRACE(...) ((void)0)
#define LOG_DEBUG(...) ((void)0)
#define LOG_INFO(...) ((void)0)
#define LOG_WARN(...) ((void)0)
#define LOG_ERROR(...) ((void)0)
#define LOG_FATAL(...) ((void)0)
#define CONDITIONAL_TRACE(X) ((void)0)

#endif // NTS_ENABLE_LOGGING
