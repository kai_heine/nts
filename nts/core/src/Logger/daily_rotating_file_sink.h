#pragma once

#ifdef NTS_ENABLE_LOGGING

#include <mutex>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/details/file_helper.h>
#include <spdlog/details/null_mutex.h>
#include <spdlog/details/synchronous_factory.h>


// file sink rotating daily and based on size
// based on rotating_file_sink
template <typename Mutex>
class daily_rotating_file_sink final : public spdlog::sinks::base_sink<Mutex>
{
public:
	daily_rotating_file_sink(spdlog::filename_t base_filename, std::size_t max_size,
							 std::size_t max_files, bool rotate_on_open = false,
							 bool rotate_daily = true);

private:
	void sink_it_(const spdlog::details::log_msg &msg) override;
	void flush_() override;

	void rotate_();
	bool rename_file(const spdlog::filename_t &src_filename, const spdlog::filename_t &target_filename);

	spdlog::filename_t base_filename_;
	std::size_t max_size_;
	std::size_t max_files_;
	std::size_t current_size_;
	spdlog::details::file_helper file_helper_;
	std::tm last_rotation_;
	bool rotate_daily_;
};

using daily_rotating_file_sink_mt = daily_rotating_file_sink<std::mutex>;
using daily_rotating_file_sink_st = daily_rotating_file_sink<spdlog::details::null_mutex>;

template <typename Factory = spdlog::synchronous_factory>
inline std::shared_ptr<spdlog::logger> daily_rotating_logger_mt(
	const std::string &logger_name, const spdlog::filename_t &filename, std::size_t max_size,
	std::size_t max_files, bool rotate_on_open = false, bool rotate_daily = true)
{
	return Factory::template create<daily_rotating_file_sink_mt>(logger_name, filename, max_size, max_files,
																 rotate_on_open, rotate_daily);
}

template <typename Factory = spdlog::synchronous_factory>
inline std::shared_ptr<spdlog::logger> daily_rotating_logger_st(
	const std::string &logger_name, const spdlog::filename_t &filename, std::size_t max_size,
	std::size_t max_files, bool rotate_on_open = false, bool rotate_daily = true)
{
	return Factory::template create<daily_rotating_file_sink_st>(logger_name, filename, max_size, max_files,
																 rotate_on_open, rotate_daily);
}

#endif
