#ifdef NTS_ENABLE_LOGGING

#include "daily_rotating_file_sink.h"
#include <spdlog/details/os.h>
#include <spdlog/common.h>
#include <spdlog/sinks/rotating_file_sink.h>


template <typename Mutex>
daily_rotating_file_sink<Mutex>::daily_rotating_file_sink(spdlog::filename_t base_filename,
														  std::size_t max_size, std::size_t max_files,
														  bool rotate_on_open, bool rotate_daily)
	: base_filename_(std::move(base_filename)),
	  max_size_(max_size),
	  max_files_(max_files),
	  rotate_daily_(rotate_daily)
{
	file_helper_.open(spdlog::sinks::rotating_file_sink<Mutex>::calc_filename(base_filename_, 0));
	current_size_ = file_helper_.size(); // expensive. called only once
	if (rotate_on_open && current_size_ > 0)
	{
		rotate_();
	}
	last_rotation_ = spdlog::details::os::localtime();
}

template <typename Mutex>
void daily_rotating_file_sink<Mutex>::sink_it_(const spdlog::details::log_msg &msg)
{
	spdlog::memory_buf_t formatted;
	spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);
	current_size_ += formatted.size();

	auto now = spdlog::details::os::localtime();

	if ((current_size_ > max_size_) ||
		(rotate_daily_ && (now.tm_yday != last_rotation_.tm_yday)))
	{
		rotate_();
		current_size_ = formatted.size();
		last_rotation_ = now;
	}
	file_helper_.write(formatted);
}

template <typename Mutex>
void daily_rotating_file_sink<Mutex>::flush_()
{
	file_helper_.flush();
}

template <typename Mutex>
void daily_rotating_file_sink<Mutex>::rotate_()
{
	using spdlog::details::os::filename_to_str;
	file_helper_.close();
	for (auto i = max_files_; i > 0; --i)
	{
		auto src = spdlog::sinks::rotating_file_sink<Mutex>::calc_filename(base_filename_, i - 1);
		if (!spdlog::details::file_helper::file_exists(src))
		{
			continue;
		}
		auto target = spdlog::sinks::rotating_file_sink<Mutex>::calc_filename(base_filename_, i);

		if (!rename_file(src, target))
		{
			// if failed try again after a small delay.
			// this is a workaround to a windows issue, where very high rotation
			// rates can cause the rename to fail with permission denied (because of antivirus?).
			spdlog::details::os::sleep_for_millis(100);
			if (!rename_file(src, target))
			{
				// truncate the log file anyway to prevent it to grow beyond its limit!
				file_helper_.reopen(true);
				current_size_ = 0;
				SPDLOG_THROW(
					spdlog::spdlog_ex("rotating_file_sink: failed renaming " +
										  filename_to_str(src) + " to " + filename_to_str(target),
									  errno));
			}
		}
	}
	file_helper_.reopen(true);
}

template <typename Mutex>
bool daily_rotating_file_sink<Mutex>::rename_file(const spdlog::filename_t &src_filename,
												  const spdlog::filename_t &target_filename)
{
	// try to delete the target file in case it already exists.
	(void)spdlog::details::os::remove(target_filename);
	return spdlog::details::os::rename(src_filename, target_filename) == 0;
}

// explicit template instantiations
template class daily_rotating_file_sink<std::mutex>;
template class daily_rotating_file_sink<spdlog::details::null_mutex>;

#endif