#include "Logger.h"

#ifdef NTS_ENABLE_LOGGING

#include "../NtsSettings.h"
#include "daily_rotating_file_sink.h"
#include <boost/filesystem.hpp>
#include <spdlog/async.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#define P_DATE "%d.%m.%Y"
#define P_TIME "%H:%M:%S.%e"
#define P_LEVEL "[%^%-5l%$]"
#define P_NAME "[%n]"
#define P_SOURCE "[%-28s] [%4#] [%-25!]"
#define P_MESSAGE "%v"

namespace
{

std::shared_ptr<spdlog::logger> createLogger()
{
	auto logger = spdlog::create_async<spdlog::sinks::stdout_color_sink_mt>("nts");
	logger->flush_on(spdlog::level::warn);
	logger->set_level(spdlog::level::trace);

	auto console_sink = logger->sinks().front();
	console_sink->set_pattern(P_TIME " " P_LEVEL " " P_NAME " " P_MESSAGE);
	console_sink->set_level(spdlog::level::trace);

	return logger;
}

spdlog::level::level_enum fromString(const std::string &lvl)
{
	std::string loglevel = lvl;
	std::transform(loglevel.begin(), loglevel.end(), loglevel.begin(),
				   [](char c) { return static_cast<char>(std::tolower(c)); });

	if (loglevel == "trace")
		return spdlog::level::level_enum::trace;
	if (loglevel == "debug")
		return spdlog::level::level_enum::debug;
	if (loglevel == "info")
		return spdlog::level::level_enum::info;
	if (loglevel == "warning")
		return spdlog::level::level_enum::warn;
	if (loglevel == "error")
		return spdlog::level::level_enum::err;
	if (loglevel == "fatal")
		return spdlog::level::level_enum::critical;

	LOG_WARN("invalid log level");
	return spdlog::level::level_enum::info;
}

} // namespace

spdlog::logger *Logger()
{
	static auto logger = createLogger();
	return logger.get();
}

void configureLogger(const NtsSettings &settings)
{
	auto logger = Logger();

	if (settings.console_logging.enableLogToConsole)
	{
		auto console_sink = logger->sinks().front();

		console_sink->set_level(fromString(settings.console_logging.logLevel));
		if (settings.console_logging.advancedLogInfos)
		{
			console_sink->set_pattern(P_TIME " " P_LEVEL " " P_NAME " " P_SOURCE " " P_MESSAGE);
		}
	}
	else
	{
		logger->sinks().clear();
	}

	if (settings.file_logging.enableLogToFile)
	{
		namespace fs = boost::filesystem;
		const fs::path filePath{settings.file_logging.logFile};

		if (fs::is_directory(filePath))
		{
			throw std::runtime_error("invalid filename");
		}
		if (!fs::exists(filePath.parent_path()))
		{
			if (!fs::create_directories(filePath.parent_path()))
			{
				throw std::runtime_error("error creating directories");
			}
		}

		auto file_sink = std::make_shared<daily_rotating_file_sink_mt>(
			settings.file_logging.logFile, settings.file_logging.maxLogFileSize,
			settings.file_logging.maxLogFiles, settings.file_logging.rotateOnStartup,
			settings.file_logging.dailyRotation);

		if (settings.file_logging.advancedLogInfos)
		{
			file_sink->set_pattern(P_DATE " " P_TIME " " P_LEVEL " " P_NAME " " P_SOURCE " " P_MESSAGE);
		}
		else
		{
			file_sink->set_pattern(P_DATE " " P_TIME " " P_LEVEL " " P_NAME " " P_MESSAGE);
		}

		logger->sinks().push_back(file_sink);
	}
}

#else
void configureLogger(const NtsSettings &){}
#endif
