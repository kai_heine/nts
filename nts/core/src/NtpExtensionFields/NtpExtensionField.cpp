#include "../NtsConfig.h"
#include "NtpExtensionField.h"
#include "../Logger/Logger.h"

constexpr std::size_t NtpExtensionField::c_maxValueSize;
constexpr std::size_t NtpExtensionField::c_extFieldMinSize;
constexpr std::size_t NtpExtensionField::c_extFieldHeaderSize;

/**
* @brief	Creates a new NTPv4 Extension Field.
* 
* @param [in]  value	The Payload of the new Extension Field. This function copies the delivered parameter.
*/
NtpExtensionField::NtpExtensionField(unsigned short fieldType, std::vector<unsigned char> const & value)
{
	this->setFieldType(fieldType);
	this->setValue(value);
}


/**
* @brief	Creates a new NTPv4 Extension Field.
*
* @param [in]  value	The Payload of the new Extension Field. This function copies the delivered parameter.
*/
NtpExtensionField::NtpExtensionField(NtpExtFieldType fieldType, std::vector<unsigned char> const & value)
{
	this->setFieldType(fieldType);
	this->setValue(value);
}


/**
* @brief	Creates a new NTPv4 Extension Field by parsing a binary data stream.
*           The minimum size is 16 bytes.
*/
NtpExtensionField::NtpExtensionField(std::vector<unsigned char> const & serializedData)
	:
	m_fieldType(0)
{
	this->fromSerializedData(serializedData);
}


unsigned short NtpExtensionField::getFieldTypeVal() const
{
	return m_fieldType;
}


NtpExtFieldType NtpExtensionField::getFieldType() const
{
	return static_cast<NtpExtFieldType>(m_fieldType);
}


void NtpExtensionField::setFieldType(unsigned short fieldType)
{
	this->m_fieldType = fieldType;
}


void NtpExtensionField::setFieldType(NtpExtFieldType fieldType)
{
	this->m_fieldType = static_cast<unsigned short>(fieldType);
}


/**
* @brief	Returns the length of the Value field (not the length of the whole Extension Field). 
*/
unsigned short NtpExtensionField::getValueLength() const
{
	return static_cast<unsigned short>(m_value.size());
}


/**
* @brief	Returns the Value of the Extension Field. This may also include padding bytes if the 
*           extension field has been parsed from binary data (see fromSerializedData()). 
*/
const std::vector<unsigned char> & NtpExtensionField::getValue() const
{
	return m_value;
}


void NtpExtensionField::setValue(std::vector<unsigned char> const & value)
{
	if (value.size() > c_maxValueSize)
	{
		LOG_ERROR("the allowable size of the Value field has been exceeded: {} bytes (max: {} bytes)", value.size(), c_maxValueSize);
		return;
	}

	this->m_value = value;
}


/**
* @brief	Serializes the NTP extension field and returns the binary stream.
*
* @param [in]  minSize	The minimum size to be returned. This should be 16 or 28 bytes (see RFC 7822 
*                       for more information). The data are zero-padded to a word (four octets) boundary. 
*/
std::vector<unsigned char> NtpExtensionField::getSerializedData(std::size_t minSize) const
{
	unsigned short baseSize = static_cast<unsigned short>(c_extFieldHeaderSize + static_cast<unsigned short>(m_value.size()));

	if (minSize > c_maxValueSize)
	{
		LOG_ERROR("invalid size of 'minSize': {} (max: {})", minSize, c_maxValueSize);
		return std::vector<unsigned char>();
	}

	unsigned short additionalBytes = 0;
	if (baseSize < minSize)
	{
		additionalBytes = static_cast<unsigned char>(minSize - baseSize);
	}

	unsigned short paddingSize = (baseSize + additionalBytes) % 4; // word boundary (four octets)
	std::vector<unsigned char> extFieldStream (c_extFieldHeaderSize);

	// write Field Type
	extFieldStream.at(0) = (m_fieldType >> 8) & 0xFF;
	extFieldStream.at(1) = m_fieldType & 0xFF;

	// write Length 
	unsigned short length = baseSize + additionalBytes + paddingSize;
	extFieldStream.at(2) = (length >> 8) & 0xFF;
	extFieldStream.at(3) = length & 0xFF;

	// write Value
	extFieldStream.insert(extFieldStream.end(), m_value.begin(), m_value.end());

	// write Padding (if necessary) 
	if ((additionalBytes + paddingSize) > 0)
	{
		std::vector<unsigned char> padding(additionalBytes + paddingSize);
		extFieldStream.insert(extFieldStream.end(), padding.begin(), padding.end());
	}

	return extFieldStream;
}


/**
* @brief	Creates a new NTPv4 Extension Field by parsing a binary data stream.
*           The minimum size is 16 bytes.
*/
void NtpExtensionField::fromSerializedData(std::vector<unsigned char> const & serializedData)
{
	if (serializedData.size() < 16)
	{
		LOG_ERROR("invalid data size: {} bytes (min: 16 bytes)", serializedData.size());
		return;
	}

	// parse binary data stream
	unsigned short fieldType = ((serializedData.at(0) & 0xFF) << 8) | serializedData.at(1);
	unsigned short valueLength = static_cast<unsigned short>(((serializedData.at(2) << 8) | serializedData.at(3)) - c_extFieldHeaderSize);
	std::vector<unsigned char> value(serializedData.begin() + 4, serializedData.end()); // may contain padding

	// check values
	if (value.size() != valueLength)
	{
		LOG_ERROR("invalid NTPv4 extension field (size mismatch).");
		return;
	}

	// all is fine -> save data
	m_value.clear();
	m_fieldType = fieldType;
	m_value = std::move(value);
}


unsigned short NtpExtensionField::getExtensionFieldLength(std::size_t minSize) const
{
	unsigned short extFieldLength = static_cast<unsigned short>(c_extFieldHeaderSize + static_cast<unsigned short>(m_value.size()));

	if (minSize > c_maxValueSize)
	{
		LOG_ERROR("invalid size of 'minSize': {} (max: {})", minSize, c_maxValueSize);
		return 0;
	}

	unsigned short additionalBytes = 0;
	if (extFieldLength < minSize)
	{
		additionalBytes = static_cast<unsigned char>(minSize - extFieldLength);
	}

	unsigned short paddingSize = (extFieldLength + additionalBytes) % 4; // word boundary (four octets)
	extFieldLength += paddingSize;

	return extFieldLength;
}


bool NtpExtensionField::isValid()
{
	if (m_value.size() < (16 - c_extFieldHeaderSize)) // The minimum size is 16 bytes (include header).
	{
		LOG_ERROR("invalid size of the Value field: {} bytes (min: 12 bytes)", m_value.size()); // TODO: Logs geh�ren NICHT in einer isXXX()-funktion
		return false;
	}

	if (m_value.size() % 4 != 0)
	{
		LOG_ERROR("invalid size of the Value field (not aligned with the word boundary).");
		return false;
	}


	return true;
}


/**
* @brief	Deletes the content.
*/
void NtpExtensionField::clear()
{
	m_fieldType = 0;
	m_value.clear();
}


/**
* @brief	Request a change in capacity.
*/
void NtpExtensionField::reserve(std::size_t bytes)
{
	m_value.shrink_to_fit();
	m_value.reserve(bytes);
}