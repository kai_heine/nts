#include "../NtsConfig.h"
#include "NtpExtensionField.h"
#include "ExtF_NtsAuthAndEncEF.h"
#include "../Logger/Logger.h"
#include "../AEAD.h"

#ifndef min
#define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

/**
* @brief	Clears the content and sets the Extension Field header according the rules of this type.
*/
void ExtField::NtsAuthAndEncEF::format(NtpExtensionField &ntpExtField)
{
	ntpExtField.clear();
	ntpExtField.setFieldType(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
}


/**
* @brief	Checks if the Extension Field is valid and compliant with the NTS specification.
*/
bool ExtField::NtsAuthAndEncEF::isValid(NtpExtensionField const &ntpExtField)
{
	bool status = true;

	if (ntpExtField.getFieldType() != NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields)
	{
		LOG_ERROR("wrong Field Type of the Extension Field: '{}'", ntpExtField.getFieldTypeVal());
		status = false;
	}
	
	if (ntpExtField.getValueLength() < 4)
	{
		LOG_ERROR("invalid size of the Extension Field: '{}'", ntpExtField.getValueLength());
		status = false;
	}

	unsigned short nonceSize = getNonceLength(ntpExtField);
	unsigned short ciphertextSize = getCiphertextLength(ntpExtField);

	std::size_t noncePad = calcNoncePadding(nonceSize);
	std::size_t ciphertextPad = calcCiphertextPadding(ciphertextSize);

	std::size_t len = nonceSize + noncePad + ciphertextSize + ciphertextPad;

	if (ntpExtField.getValueLength() < len)
	{
		LOG_ERROR("invalid size of the Extension Field: '{}'", ntpExtField.getValueLength());
		status = false;
	}

	return status;
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @param [in]	plaintext	Contains all NTP Extension Fields to be encrypted.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField,
											AeadAlgorithm aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & plaintext, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	// perform some checks
	if (nonce.size() == 0)
	{
		LOG_ERROR("invalid size of 'nonce'");
		return false;
	}

	if (associatedData.size() == 0)
	{
		LOG_ERROR("invalid size of 'associatedData'");
		return false;
	}

	// AEAD operation
	std::vector<unsigned char> aeadOut;
	if (AEAD::directEncrypt(aeadAlgorithm, key, plaintext, aeadOut, nonce, associatedData) == false)
	{
		LOG_ERROR("AEAD operation failed");
		return false;
	}

	std::size_t noncePadding = calcNoncePadding(nonce.size());
	std::size_t ciphertextPadding = calcCiphertextPadding(aeadOut.size());
	std::size_t additionalPadding = calcAdditionalPadding(aeadAlgorithm, nonce.size());


	std::vector<unsigned char> content(4); // 4: Nonce Length field + Ciphertext Length field
	std::size_t contentSize;
	contentSize = 4 + nonce.size() + noncePadding + aeadOut.size() + ciphertextPadding + additionalPadding; // 4 bytes: both length fields
	content.reserve(contentSize);

	
	// write 'Nonce Length'
	std::size_t nonceLength = static_cast<unsigned short>(nonce.size());
	content.at(0) |= (nonceLength >> 8) & 0xFF;
	content.at(1) |= nonceLength & 0xFF;

	// write 'Ciphertext Length'
	std::size_t ciphertextLength = static_cast<unsigned short>(aeadOut.size());
	content.at(2) |= (ciphertextLength >> 8) & 0xFF;
	content.at(3) |= ciphertextLength & 0xFF;


	// write 'Nonce'
	content.insert(content.end(), nonce.begin(), nonce.end());
	if (noncePadding != 0)
	{
		content.resize(content.size() + noncePadding);
	}

	// write 'Ciphertext'
	content.insert(content.end(), aeadOut.begin(), aeadOut.end());
	if (ciphertextPadding != 0)
	{
		content.resize(content.size() + ciphertextPadding);
	}
	

	// write 'Additional Padding'
	if (additionalPadding != 0)
	{
		content.resize(content.size() + additionalPadding);
	}


	// TODO: direkt beschreiben anstatt copy?
	// fill extension field
	// LOG_INFO("size: " << content.size() << "\t  capacity: " << content.capacity();
	ntpExtField.setValue(content);

	return true;
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @param [in]	plaintext	Contains all NTP Extension Fields to be encrypted.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											unsigned short aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & plaintext, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, static_cast<AeadAlgorithm>(aeadAlgorithm), key, plaintext, nonce, associatedData);
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											AeadAlgorithm aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, aeadAlgorithm, key, std::vector<unsigned char>(), nonce, associatedData);
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											unsigned short aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, static_cast<AeadAlgorithm>(aeadAlgorithm), key, std::vector<unsigned char>(), nonce, associatedData);
}


unsigned short ExtField::NtsAuthAndEncEF::getNonceLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	std::size_t contentSize = rawContent.size();

	if (contentSize < 2) // size of the 'Nonce Length' field (2 bytes)
	{
		LOG_ERROR("invalid size of the 'Nonce Length' field");
		return 0;
	}

	unsigned short nonceLength = (rawContent.at(0) << 8) | rawContent.at(1);
	return nonceLength;
}


/*
  returns the nonce without any padding
*/
std::vector<unsigned char> ExtField::NtsAuthAndEncEF::getNonce(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	std::size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short dataLength = 4 + nonceLength; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid nonce length");
		return std::vector<unsigned char>();
	}

	std::vector<unsigned char> nonce(rawContent.begin() + 4, rawContent.begin() + dataLength);
	return nonce;
}


unsigned short ExtField::NtsAuthAndEncEF::getCiphertextLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	std::size_t contentSize = rawContent.size();

	if (contentSize < 4) // 4 bytes: both length fields
	{
		LOG_ERROR("invalid size of the 'Ciphertext Length' field");
		return 0;
	}

	unsigned short ciphertextLength = (rawContent.at(2) << 8) | rawContent.at(3);
	return ciphertextLength;
}


std::vector<unsigned char> ExtField::NtsAuthAndEncEF::getCiphertext(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	std::size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short noncePadding = 0;

	if (nonceLength % 4 != 0)
	{
		noncePadding = 4 - (nonceLength % 4);
	}

	unsigned short ciphertextLength = getCiphertextLength(ntpExtField);
	unsigned short dataLength = 4 + nonceLength + noncePadding + ciphertextLength; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid size of the 'Ciphertext' field");
		return std::vector<unsigned char>();
	}

	unsigned int startPos = 4 + nonceLength + noncePadding;
	unsigned int endPos = dataLength;

	std::vector<unsigned char> ciphertext(rawContent.begin() + startPos, rawContent.begin() + endPos);
	return ciphertext;
}


unsigned short ExtField::NtsAuthAndEncEF::getPaddingLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	std::size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short noncePadding = 0;

	if (nonceLength % 4 != 0)
	{
		noncePadding = 4 - (nonceLength % 4);
	}

	unsigned short ciphertextLength = getCiphertextLength(ntpExtField);
	unsigned short ciphertextPadding = 0;

	if (ciphertextLength % 4 != 0)
	{
		ciphertextPadding = 4 - (ciphertextLength % 4);
	}

	unsigned short dataLength = 4 + nonceLength + noncePadding + ciphertextLength + ciphertextPadding; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid size of the extension field");
		return 0;
	}

	return static_cast<unsigned short>(contentSize - dataLength);
}

std::size_t ExtField::NtsAuthAndEncEF::calcNoncePadding(std::size_t nonceSize)
{
	std::size_t paddingSize = 0;
	if (nonceSize % 4 != 0)
	{
		paddingSize = 4 - (nonceSize % 4);
	}
	return paddingSize;
}

std::size_t ExtField::NtsAuthAndEncEF::calcCiphertextPadding(std::size_t ciphertextSize)
{
	std::size_t paddingSize = 0;
	if (ciphertextSize % 4 != 0)
	{
		paddingSize = 4 - (ciphertextSize % 4);
	}
	return paddingSize;
}


std::size_t ExtField::NtsAuthAndEncEF::calcAdditionalPadding(AeadAlgorithm aeadAlgorithm, std::size_t nonceSize)
{
	std::size_t N_MAX = 0;
	if (AEAD::isSupported(aeadAlgorithm) == true)
	{
		N_MAX = AEAD::getMaxNonceLength(aeadAlgorithm);
	}
	else
	{
		LOG_ERROR("AeadAlgorithm is not supported");
		return 0;
	}

	std::size_t noncePadding = calcNoncePadding(nonceSize);
	std::size_t N_LEN = nonceSize + noncePadding;
	std::size_t N_REQ = min(16, N_MAX);  // 16: defined in nts4ntp spec

	if (N_REQ % 4 != 0) // add padding
	{
		N_REQ += 4 - (N_REQ % 4);
	}

	std::size_t additionalPadding = 0;
	if (N_LEN >= N_REQ)
	{
		additionalPadding = 0;
	}
	else
	{
		additionalPadding = N_REQ - N_LEN;
	}
	return additionalPadding;
}



std::size_t ExtField::NtsAuthAndEncEF::calculateContentSize(AeadAlgorithm aeadAlgorithm, std::size_t plaintextSize, std::size_t nonceSize)
{
	
	std::size_t ciphertextSize = AEAD::getAuthTagLen(aeadAlgorithm) + plaintextSize;  // TODO: sind andere gr��en m�glich?
	std::size_t noncePad = calcNoncePadding(nonceSize);
	std::size_t ciphertextPad = calcCiphertextPadding(ciphertextSize);
	std::size_t additionalPad = calcAdditionalPadding(aeadAlgorithm, nonceSize);
	std::size_t contentSize = 4 + nonceSize + noncePad + ciphertextSize + ciphertextPad + additionalPad; // 4 bytes: both length fields (Nonce Length and Ciphertext Length)
	return contentSize;
}



bool ExtField::NtsAuthAndEncEF::verifyContentSize(NtpExtensionField const& ntpExtField, AeadAlgorithm aeadAlgorithm)
{
	if (ntpExtField.getValueLength() < 4)
	{
		return false;
	}

	if (AEAD::isSupported(aeadAlgorithm) == false)
	{
		return false;
	}

	std::size_t contentSize = calculateContentSize(aeadAlgorithm, getCiphertextLength(ntpExtField), getNonceLength(ntpExtField));

	if (ntpExtField.getValueLength() != contentSize)
	{
		LOG_ERROR("invalid size of the Extension Field: '{}' (expected: {})", ntpExtField.getValueLength(), contentSize);
		return false;
	}
	return true;
}


