#include "HKDF.h"
#include "cryptoFunctions.h"
#include "./Logger/Logger.h"
#include <openssl/kdf.h>
#include <openssl/evp.h>
#include "NtsConfig.h"

HKDF::HKDF()
	:
	m_inputKeyMaterial(),
	m_salt(),
	m_contextInfo(),
	m_outputKey(),
	m_msgDigestAlgo(MessageDigestAlgo::Not_Defined),
	m_keyOutputLength(0)
{
}


HKDF::~HKDF()
{
	secureErase(m_inputKeyMaterial);
	secureErase(m_outputKey);
}


/**
* @brief	Sets the Input Keying Material as a copy of the passed parameter.
*/
void HKDF::setInputKeyMaterial(std::vector<unsigned char> const & inputKeyMaterial)
{
	secureErase(m_inputKeyMaterial);
	this->m_inputKeyMaterial = inputKeyMaterial;
}


const std::vector<unsigned char>& HKDF::getInputKeyMaterial() const
{
	return m_inputKeyMaterial;
}


/**
* @brief	Sets the Salt as a copy of the passed parameter.
*/
void HKDF::setSalt(std::vector<unsigned char> const & salt)
{
	this->m_salt = salt;
}


const std::vector<unsigned char>& HKDF::getSalt() const
{
	return m_salt;
}


/**
* @brief	Sets the Context Info as a copy of the passed parameter. The total 
*           length of the Context Info must not exceed 1024 bytes.
*/
void HKDF::setContextInfo(std::vector<unsigned char> const & contextInfo)
{
	if (contextInfo.size() > 1024)
	{
		LOG_WARN("the length of the Context Info exceeds the limit of 1024 bytes");
	}

	this->m_contextInfo = contextInfo;
}


const std::vector<unsigned char>& HKDF::getContextInfo() const
{
	return m_contextInfo;
}


void HKDF::setMessageDigestAlgorithm(MessageDigestAlgo msgDigestAlgo)
{
	this->m_msgDigestAlgo = msgDigestAlgo;
}


MessageDigestAlgo HKDF::getMessageDigestAlgorithm() const
{
	return MessageDigestAlgo();
}


/**
* @brief	Sets the length of the Output Keying Material. The max. length of 
*           the Output Keying Material is (255 * HashLength). If the parameter 
*           passed is zero, then the output key length is the same as the input 
*           key length.
*/
void HKDF::setKeyOutputLength(std::size_t keyOutputLength)
{
	this->m_keyOutputLength = keyOutputLength;
}


std::size_t HKDF::getKeyOutputLength() const
{
	return m_keyOutputLength;
}


/**
* @brief	Returns the derived input key (after :performHKDF()).
*/
const std::vector<unsigned char>& HKDF::getDerivedKey() const
{
	return m_outputKey;
}


/**
* @brief	Derives the input key and returns 'true' if the operation was successful.
*           The function getDerivedKey() can be used to get the output key after this
*           operation.
*/
bool HKDF::performHKDF()
{
	// some checks
	if (m_inputKeyMaterial.size() == 0)
	{
		LOG_ERROR("the Input Keying Material is not set");
		return false;
	}
	if (m_msgDigestAlgo == MessageDigestAlgo::Not_Defined)
	{
		LOG_ERROR("the Message Digest algorithm is not set");
		return false;
	}
	if (m_keyOutputLength == 0)
	{
		m_keyOutputLength = m_inputKeyMaterial.size();
	}

	// initiate derivation process
	EVP_PKEY_CTX *pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_HKDF, NULL);
	if (pctx == nullptr)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_CTX_new_id()'");
		return false;
	}

	if (EVP_PKEY_derive_init(pctx) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_derive_init()'");
		return false;
	}

	// set parameters
	const EVP_MD *msgDigestAlgo = EVP_get_digestbynid(static_cast<int>(m_msgDigestAlgo));
	if (msgDigestAlgo == nullptr)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_get_digestbynid()'");
		return false;
	}
	if (EVP_PKEY_CTX_set_hkdf_md(pctx, msgDigestAlgo) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_CTX_set_hkdf_md()'");
		return false;
	}

	if (EVP_PKEY_CTX_set1_hkdf_salt(pctx, m_salt.data(), static_cast<int>(m_salt.size())) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_CTX_set1_hkdf_salt()'");
		return false;
	}

	if (EVP_PKEY_CTX_set1_hkdf_key(pctx, m_inputKeyMaterial.data(), static_cast<int>(m_inputKeyMaterial.size())) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_CTX_set1_hkdf_key()'");
		return false;
	}

	if (EVP_PKEY_CTX_add1_hkdf_info(pctx, m_contextInfo.data(), static_cast<int>(m_contextInfo.size())) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_CTX_add1_hkdf_info()'");
		return false;
	}

	// derived key
	secureErase(m_outputKey);
	std::size_t outLength = m_keyOutputLength;
	m_outputKey.resize(outLength);
	if (EVP_PKEY_derive(pctx, m_outputKey.data(), &outLength) <= 0)
	{
		LOG_ERROR("key derivation failed: error in 'EVP_PKEY_derive()'");
		secureErase(m_outputKey);
		return false;
	}
	if (m_outputKey.size() > outLength)
	{
		LOG_WARN("the Output Keying Material is smaller than the defined length");
		m_outputKey.resize(outLength);
	}
	return true;
}


/**
* @brief	Clears the content (keys and settings)
*
* @param [in]	keysOnly		Clears the Input Keying Material and the Output Keying Material only.
*                               The remaining settings (Salt, Context Info, key output length, 
*                               MD algorithm) are retained.
*
* @param [in]	secureErase		Overwrites the data with random values before deleting.
*/
void HKDF::clear(bool keysOnly, bool secureErase)
{
	if (secureErase == true)
	{
		::secureErase(m_inputKeyMaterial);
		::secureErase(m_outputKey);

		if (keysOnly == false)
		{
			::secureErase(m_salt);
			::secureErase(m_contextInfo);
			m_msgDigestAlgo = MessageDigestAlgo::Not_Defined;
			m_keyOutputLength = 0;
		}
	}
	else
	{
		m_inputKeyMaterial.clear();
		m_outputKey.clear();

		if (keysOnly == false)
		{
			m_salt.clear();
			m_contextInfo.clear();
			m_msgDigestAlgo = MessageDigestAlgo::Not_Defined;
			m_keyOutputLength = 0;
		}
	}
}
