#### External Dependencies ####

# always link Boost statically
set(Boost_USE_STATIC_LIBS ON CACHE BOOL "" FORCE)
find_package(Boost 1.67 REQUIRED COMPONENTS system filesystem date_time regex thread)

find_package(OpenSSL 1.1.1 REQUIRED)
find_package(Threads)


#### NTS Library ####
add_library(nts_core OBJECT
	src/Cookie/CookieContent.cpp
	src/Cookie/Cookie.cpp

	src/Logger/Logger.cpp
	src/Logger/daily_rotating_file_sink.cpp

	src/NtpExtensionFields/ExtF_NtsAuthAndEncEF.cpp
	src/NtpExtensionFields/ExtF_NtsCookie.cpp
	src/NtpExtensionFields/ExtF_NtsCookiePlaceholder.cpp
	src/NtpExtensionFields/ExtF_UniqueIdentifier.cpp
	src/NtpExtensionFields/NtpExtensionField.cpp
	src/NtpExtensionFields/NtpExtFieldStack.cpp
	src/NtpExtensionFields/NtpExtFieldStackVerification.cpp

	src/NtsClient/ClientState.cpp
	src/NtsClient/NtsClient.cpp
	src/NtsClient/NtsClientKe.cpp
	src/NtsClient/NtsClientNtp.cpp
	src/NtsClient/NtsClientSession.cpp
	src/NtsClient/TlsClient.cpp

	src/NtsServer/NtsServer.cpp
	src/NtsServer/NtsServerKe.cpp
	src/NtsServer/NtsServerNtp.cpp
	src/NtsServer/ServerState.cpp
	src/NtsServer/TlsServer.cpp
	src/NtsServer/TlsServerSession.cpp

	src/Records/Record.cpp
	src/Records/RecordStack.cpp
	src/Records/RecStackVerification.cpp
	src/Records/RecType_AeadAlgoNeg.cpp
	src/Records/RecType_EndOfMessage.cpp
	src/Records/RecType_Error.cpp
	src/Records/RecType_NewCookie.cpp
	src/Records/RecType_NextProtNeg.cpp
	src/Records/RecType_Ntpv4ServerNeg.cpp
	src/Records/RecType_Ntpv4PortNeg.cpp
	src/Records/RecType_Warning.cpp

	src/AEAD.cpp
	src/ConfigFile.cpp
	src/cryptoFunctions.cpp
	src/generalFunctions.cpp
	src/HKDF.cpp
	src/MasterKey.cpp
	src/NtpPacket.cpp
	src/NtpPacketInfoImpl.cpp
	src/NtsSettings.cpp
	src/NtsUnicastServiceImpl.cpp
)
target_include_directories(nts_core PUBLIC src)

target_link_libraries(nts_core
	PUBLIC
		Boost::system
		Boost::filesystem
		Boost::date_time
		Boost::regex
		Boost::thread
		$<$<BOOL:${NTS_ENABLE_LOGGING}>:spdlog::spdlog>
		Threads::Threads
		aes_siv
		OpenSSL::SSL
		OpenSSL::Crypto
)

target_compile_features(nts_core PUBLIC cxx_std_14)
set_target_properties(nts_core PROPERTIES CXX_EXTENSIONS OFF) # disable compiler-specific extensions for portability

target_compile_definitions(nts_core PUBLIC
	$<$<CONFIG:Debug>:_DEBUG>
	$<$<BOOL:${NTS_ENABLE_LOGGING}>:NTS_ENABLE_LOGGING>
	$<$<BOOL:${NTS_ENABLE_TRACE_LOGGING}>:NTS_ENABLE_TRACE_LOGGING>
)
