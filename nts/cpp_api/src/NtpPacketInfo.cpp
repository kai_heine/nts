#include "NtpPacketInfo.h"
#include "NtpPacketInfoImpl.h"
#include "Logger/Logger.h"
#include <cstddef>

NtpPacketInfo::NtpPacketInfo()
	:
	m_ntpPacketInfoImpl(new NtpPacketInfoImpl())
{
}


NtpPacketInfo::~NtpPacketInfo()
{
}


void NtpPacketInfo::setServer(std::string const& ntsKeServerIp, std::string const& ntpTimeServerIp, unsigned short ntpTimeServerPort)
{
	m_ntpPacketInfoImpl->setServer(ntsKeServerIp, ntpTimeServerIp, ntpTimeServerPort);
}


std::string const & NtpPacketInfo::getTimeServerIp() const
{
	return m_ntpPacketInfoImpl->getNtpTimeServerIp();
}


unsigned short NtpPacketInfo::getTimeServerPort() const
{
	return m_ntpPacketInfoImpl->getNtpTimeServerPort();
}


int NtpPacketInfo::getLastErrorCode() const
{
	return m_ntpPacketInfoImpl->getErrorCode();
}


std::string NtpPacketInfo::getErrorStr(int errorCode) const
{
	(void)errorCode;
	return std::string("---not yet supported---"); // TODO: definieren
}


std::size_t NtpPacketInfo::getRequiredNtsSpace() const
{
	return m_ntpPacketInfoImpl->getNtsContentLengthNeeded();
}


std::size_t NtpPacketInfo::getCurrentNtsSpace() const
{
	return m_ntpPacketInfoImpl->getCurrentNtsContentLength();
}

