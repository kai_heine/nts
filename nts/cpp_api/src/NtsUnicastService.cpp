#include "BaseConfig.h" // muss an erster stelle stehen (avoid warning: -D_WIN32_WINNT=0x0501)
#include "NtsUnicastService.h"
#include "NtsUnicastServiceImpl.h"
#include "NtpPacketInfo.h"
#include "NtpPacketInfoImpl.h"

NtsUnicastService::NtsUnicastService() // todo: benötigt?  hierfür eine zusätzliche initialization()-Methode definieren
	: 
	m_ntsUnicastServiceImpl(new NtsUnicastServiceImpl())
{
}

NtsUnicastService::~NtsUnicastService() = default;


void NtsUnicastService::initialize(std::string const &globalConfigFile)
{
	m_ntsUnicastServiceImpl->initialize(globalConfigFile);
	return;
}


int NtsUnicastService::processAndVerify(NtpPacketInfo & ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen)
{
	if ((ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}

	ntpPacketInfo.m_ntpPacketInfoImpl->setNtpPacket(ntpPacket, packetLen, bufferSize, true);
	m_ntsUnicastServiceImpl->processAndVerify(*ntpPacketInfo.m_ntpPacketInfoImpl.get());
	if (ntpPacketInfo.getLastErrorCode() == 0) // TODO: fehlercode darf nicht pos. sein. --> ggf anpassen
	{
		return static_cast<int>(ntpPacketInfo.getCurrentNtsSpace());
	}
	else
	{
		return ntpPacketInfo.getLastErrorCode();
	}
}


int NtsUnicastService::createMessage_init(NtpPacketInfo & ntpPacketInfo)
{
	m_ntsUnicastServiceImpl->createMessage_init(*ntpPacketInfo.m_ntpPacketInfoImpl.get());
	if (ntpPacketInfo.getLastErrorCode() == 0) // TODO: fehlercode darf nicht pos. sein. --> ggf anpassen
	{
		return static_cast<int>(ntpPacketInfo.getRequiredNtsSpace());
	}
	else
	{
		return ntpPacketInfo.getLastErrorCode();
	}
}


int NtsUnicastService::createMessage_update(NtpPacketInfo & ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen)
{
	if ((ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}

	ntpPacketInfo.m_ntpPacketInfoImpl->setNtpPacket(ntpPacket, packetLen, bufferSize, true);
	m_ntsUnicastServiceImpl->createMessage_update(*ntpPacketInfo.m_ntpPacketInfoImpl.get());
	if (ntpPacketInfo.getLastErrorCode() == 0) // TODO: fehlercode darf nicht pos. sein. --> ggf anpassen
	{
		return static_cast<int>(ntpPacketInfo.m_ntpPacketInfoImpl.get()->getNtpPacket().getNtpPacketLength());
	}
	else
	{
		return ntpPacketInfo.getLastErrorCode();
	}
}


int NtsUnicastService::createMessage_final(NtpPacketInfo & ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen, bool rescanNtpPacket)
{
	if ((ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}

	ntpPacketInfo.m_ntpPacketInfoImpl->setNtpPacket(ntpPacket, packetLen, bufferSize, rescanNtpPacket);
	m_ntsUnicastServiceImpl->createMessage_final(*ntpPacketInfo.m_ntpPacketInfoImpl.get());
	if (ntpPacketInfo.getLastErrorCode() == 0) // TODO: fehlercode darf nicht pos. sein. --> ggf anpassen
	{
		return static_cast<int>(ntpPacketInfo.m_ntpPacketInfoImpl.get()->getNtpPacket().getNtpPacketLength());
	}
	else
	{
		return ntpPacketInfo.getLastErrorCode();
	}
}


bool NtsUnicastService::isInit()
{
	return m_ntsUnicastServiceImpl->isInit();
}


bool NtsUnicastService::isClientMode()
{
	return m_ntsUnicastServiceImpl->isClientMode();
}


bool NtsUnicastService::isServerMode()
{
	return m_ntsUnicastServiceImpl->isServerMode();
}


void NtsUnicastService::showInfos()
{
	return m_ntsUnicastServiceImpl->showInfos();
}
