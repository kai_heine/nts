/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <memory>
#include <string>

class NtpPacketInfo;
struct NtsUnicastServiceImpl;

class NtsUnicastService
{
public:
	NtsUnicastService();
	~NtsUnicastService();
	NtsUnicastService(NtsUnicastService const&) = delete;
	NtsUnicastService& operator=(NtsUnicastService const&) = delete;

	void initialize(std::string const& globalConfigFile);

	// TODO: durch '&std::vector<unsigned char> ntpPacket' ersetzen
	int processAndVerify(NtpPacketInfo& ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen);
	int createMessage_init(NtpPacketInfo& ntpPacketInfo);
	int createMessage_update(NtpPacketInfo& ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen);
	int createMessage_final(NtpPacketInfo& ntpPacketInfo, unsigned char* ntpPacket, std::size_t bufferSize, std::size_t packetLen, bool rescanNtpPacket);

	bool isInit();
	bool isClientMode();
	bool isServerMode();
	void showInfos();

private:
	const std::unique_ptr<NtsUnicastServiceImpl> m_ntsUnicastServiceImpl;
};
