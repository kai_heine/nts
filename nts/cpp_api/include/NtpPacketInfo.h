/* 
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <string>
#include <memory>
#include <cstddef>

struct NtpPacketInfoImpl;
class NtpPacketInfo
{
public:
	NtpPacketInfo();
	~NtpPacketInfo();

	void setServer(std::string const& ntsKeServerIp, std::string const& ntpTimeServerIp, unsigned short ntpTimeServerPort);

	std::string const & getTimeServerIp() const;
	unsigned short getTimeServerPort() const;

	int getLastErrorCode() const;
	std::string getErrorStr(int errorCode) const;

	std::size_t getRequiredNtsSpace() const;
	std::size_t getCurrentNtsSpace() const;

private:
	friend class NtsUnicastService;
	const std::unique_ptr<NtpPacketInfoImpl> m_ntpPacketInfoImpl;
};

