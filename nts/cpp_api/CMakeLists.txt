add_library(nts_cpp
	src/NtsUnicastService.cpp
	src/NtpPacketInfo.cpp
)

target_include_directories(nts_cpp
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<INSTALL_INTERFACE:include>
)

target_compile_features(nts_cpp PUBLIC cxx_std_14)

target_link_libraries(nts_cpp PUBLIC nts_core aes_siv)


include(GNUInstallDirs)
install(TARGETS nts_cpp
	ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
	LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
	RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}
)
install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
