/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#ifndef NTP_PACKET_INFO_C_H
#define NTP_PACKET_INFO_C_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct NtpPacketInfoImpl;
typedef struct NtpPacketInfoImpl PACKET_INFO;

PACKET_INFO *PACKET_INFO_new();
void PACKET_INFO_free(PACKET_INFO *packet_info);

void PACKET_INFO_set_server(PACKET_INFO *packet_info, const char *ntsKeServerIp, const char *ntpTimeServerIp, unsigned short ntpTimeServerPort);
const char* PACKET_INFO_get_time_server_ip(PACKET_INFO *packet_info);
unsigned short PACKET_INFO_get_time_server_port(PACKET_INFO *packet_info);

int PACKET_INFO_get_last_error_code(PACKET_INFO *packet_info);
const char* PACKET_INFO_get_error_str(PACKET_INFO *packet_info, int error_code);

size_t PACKET_INFO_get_required_nts_space(PACKET_INFO *packet_info);
size_t PACKET_INFO_get_current_nts_space(PACKET_INFO *packet_info);

#ifdef __cplusplus
}
#endif

#endif /* NTP_PACKET_INFO_C_H */