/*
 * ----------------------------------------------------------------------------
 * Copyright 2019 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#ifndef NTS_UNICAST_SERVICE_C_H
#define NTS_UNICAST_SERVICE_C_H

#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

struct NtpPacketInfoImpl;
typedef struct NtpPacketInfoImpl PACKET_INFO;

struct NtsUnicastServiceImpl;
typedef struct NtsUnicastServiceImpl NTS_CTX;


NTS_CTX *NTS_CTX_new();
void NTS_CTX_free(NTS_CTX *ctx);



/*
	ret:	  0: success
			 -1: error (invalid parameter)
			 -2: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_init(NTS_CTX *ctx, const char *globalConfigFile);



/*
	ret:	  0: success
			 -1: error (invalid parameter)
			 -2: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_show_infos(NTS_CTX *ctx);



/*
	ret:	>=0: success (NTS integrity check pass); current size of the NTS content in the NTP packet.
			     This content will be overwritten when creating a NTS request/response.
			 -1: error (invalid parameter)
			 -2: NTS verification: integrity check not pass
			 -3: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_process_and_verify(NTS_CTX *ctx, PACKET_INFO *packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen);



/*
	ret:	>=0: success; required space for the NTS content.
				 Info: calculate NTP buffer size: 
				 bufLen = <NTP header size> + <non NTS Extension Field sizes (if any)> + <required space for NTS content>
			 -1: error (invalid parameter)
			 -2: NTS operation failed
			 -3: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_create_message_init(NTS_CTX *ctx, PACKET_INFO *packetInfo);



/*
	ret:	>=0: success; current size of the NTP packet (it is smaller than the required space for the NTS content, because NTS is not final yet)
			-1: error (invalid parameter)
			-2: NTS operation failed
			-3: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_create_message_update(NTS_CTX *ctx, PACKET_INFO *packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen);



/*
	rescanNtpPacket: true:   this option scan the given ntp packet for new Extension Fields and parses them (only needed 
                             if non-NTS Extension Fields were added or deleted after 'NTS_create_message_update()')
			
	ret:	>=0: success; current size of the NTP packet
			-1: error (invalid parameter)
			-2: NTS operation failed
			-3: critical NTS error / exception (e.g.: unable to allocate space)
*/
int NTS_create_message_final(NTS_CTX *ctx, PACKET_INFO *packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen, bool rescanNtpPacket);



#ifdef __cplusplus
}
#endif

#endif /* NTS_UNICAST_SERVICE_C_H */