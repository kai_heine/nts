#include "NtpPacketInfo_C.h"
#include "NtpPacketInfoImpl.h"
#include <stddef.h>

PACKET_INFO *PACKET_INFO_new()
{
	return new NtpPacketInfoImpl();
}


void PACKET_INFO_free(PACKET_INFO *packet_info)
{
	if (packet_info != nullptr)
	{
		delete packet_info;
	}
}


void PACKET_INFO_set_server(PACKET_INFO *packet_info, const char *ntsKeServerIp, const char *ntpTimeServerIp, unsigned short ntpTimeServerPort)
{
	packet_info->setServer((ntsKeServerIp == nullptr ? "" : ntsKeServerIp), (ntpTimeServerIp == nullptr ? "" : ntpTimeServerIp), ntpTimeServerPort);
}


const char* PACKET_INFO_get_time_server_ip(PACKET_INFO *packet_info)
{
	return packet_info->getNtpTimeServerIp().c_str();
}


unsigned short PACKET_INFO_get_time_server_port(PACKET_INFO *packet_info)
{
	return packet_info->getNtpTimeServerPort();
}


int PACKET_INFO_get_last_error_code(PACKET_INFO *packet_info)
{
	return packet_info->getErrorCode();
}


const char* PACKET_INFO_get_error_str(PACKET_INFO *packet_info, int error_code)
{
	(void)packet_info;
	(void)error_code;
	return "---not yet supported---"; // TODO: definieren
}


size_t PACKET_INFO_required_nts_space(PACKET_INFO *packet_info)
{
	return packet_info->getNtsContentLengthNeeded();
}


size_t PACKET_INFO_current_nts_space(PACKET_INFO *packet_info)
{
	return packet_info->getCurrentNtsContentLength();
}

