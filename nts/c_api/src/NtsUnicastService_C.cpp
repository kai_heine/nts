#include "BaseConfig.h" // muss an erster stelle stehen (avoid warning: -D_WIN32_WINNT=0x0501)
#include "NtsUnicastService_C.h"
#include "NtsUnicastServiceImpl.h"
#include "NtpPacketInfoImpl.h"


NTS_CTX *NTS_CTX_new()
{
	return new NtsUnicastServiceImpl();
}


void NTS_CTX_free(NTS_CTX *ctx)
{
	if (ctx != nullptr)
	{
		delete ctx;
	}
}


int NTS_init(NTS_CTX *ctx, const char *globalConfigFile)
{
	if (ctx == nullptr)
	{
		return -1;
	}

	try
	{
		ctx->initialize(globalConfigFile);
	}
	catch (...)
	{
		return -2;
	}

	return 0;
}


int NTS_show_infos(NTS_CTX *ctx)
{
	if (ctx == nullptr)
	{
		return -1;
	}

	try
	{
		ctx->showInfos();
	}
	catch (...)
	{
		return -2;
	}
	
	return 0;
}


int NTS_process_and_verify(NTS_CTX * ctx, PACKET_INFO * packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen)
{
	if ((ctx == nullptr) || (packetInfo == nullptr) || (ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}

	try
	{
		packetInfo->setNtpPacket(ntpPacket, packetLen, bufferSize, true);
		ctx->processAndVerify(*packetInfo);
		if (packetInfo->getErrorCode() == 0)
		{
			return static_cast<int>(packetInfo->getCurrentNtsContentLength());
		}
		else
		{
			return -2;
		}
	}
	catch (...)
	{
		return -3;
	}

}


int NTS_create_message_init(NTS_CTX * ctx, PACKET_INFO * packetInfo)
{
	if ((ctx == nullptr) || (packetInfo == nullptr))
	{
		return -1;
	}

	try
	{
		ctx->createMessage_init(*packetInfo);
		if (packetInfo->getErrorCode() == 0)
		{
			return static_cast<int>(packetInfo->getNtsContentLengthNeeded());
		}
		else
		{
			return -2;
		}
	}
	catch (...)
	{
		return -3;
	}
}


int NTS_create_message_update(NTS_CTX *ctx, PACKET_INFO *packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen)
{
	if ((ctx == nullptr) || (packetInfo == nullptr) || (ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}
	
	try
	{
		packetInfo->setNtpPacket(ntpPacket, packetLen, bufferSize, true);
		ctx->createMessage_update(*packetInfo);
		if (packetInfo->getErrorCode() == 0)
		{
			return static_cast<int>(packetInfo->getNtpPacket().getNtpPacketLength());
		}
		else
		{
			return -2;
		}
	}
	catch (...)
	{
		return -3;
	}
}


int NTS_create_message_final(NTS_CTX *ctx, PACKET_INFO *packetInfo, unsigned char* ntpPacket, size_t bufferSize, size_t packetLen, bool rescanNtpPacket)
{
	if ((ctx == nullptr) || (packetInfo == nullptr) || (ntpPacket == nullptr) || (packetLen < 48) || (bufferSize < packetLen)) // ntp header size
	{
		return -1;
	}

	try
	{
		packetInfo->setNtpPacket(ntpPacket, packetLen, bufferSize, rescanNtpPacket);
		ctx->createMessage_final(*packetInfo);
		if (packetInfo->getErrorCode() == 0)
		{
			return static_cast<int>(packetInfo->getNtpPacket().getNtpPacketLength());
		}
		else
		{
			return -2;
		}
	}
	catch (...)
	{
		return -3;
	}
}
